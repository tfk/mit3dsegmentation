% Quick script to take a multipage tif and explode it into multiple images.

%outputFileName = '/scratch/3dsegstack3/unified-labels.tif'
%for K=1:length(I(1,1,:))
%  imwrite(I(:,:,K), outputFileName, 'WriteMode', 'append');
%end

for i=1:100
  outputFileName = strcat('/scratch/labels_input/input-labels-', int2str(i), '.tif');
  C=[];
  C = cat(3,C,imread('/scratch/rhoana/train-labels.tif',i));
  imwrite(C, outputFileName, 'WriteMode', 'overwrite');
end
