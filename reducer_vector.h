// -*- C++ -*-
// Copyright (c) 2010, Tao B. Schardl
/*
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef REDUCER_VECTOR_H
#define REDUCER_VECTOR_H

#include <stdlib.h>
#include <assert.h>
#include <cilk/cilk.h>
#include <cilk/reducer.h>

#include <cilk/cilk_api.h>

template <typename T>
class Vector_reducer
{
public:
  struct Monoid: cilk::monoid_base< std::list<std::vector<T>* > >
  {
    static void reduce (std::list<std::vector<T>* >* left, std::list<std::vector<T>* >* right) {
      left->merge(*right);
    }
    static void identity (std::list<std::vector<T>* >* id) {
      std::vector<T>* v = new std::vector<T>();
      id->push_back(v);
    }
  };
private:
  cilk::reducer<Monoid> imp_;
  cilk::reducer_list_append<std::vector<T>* > list_reducer;
public:
  Vector_reducer();
  void insert(T);
  std::vector<T> &get_reference();
  std::vector<T> &get_reference_serial();
  inline uint numElements() const;
  inline bool isEmpty() const;
  inline void clear();
  std::vector<T>** vectors;
};

/*template <typename T>
Vector_reducer<T>::Vector_reducer(){
}
*/

template <typename T>
void
Vector_reducer<T>::insert(T el)
{
  if (list_reducer.get_reference().size() == 0) {
    list_reducer.get_reference().push_back(new std::vector<T>());
  }
  list_reducer.get_reference().back()->push_back(el);
}

/*
template <typename T>
std::vector<T>&
Vector_reducer<T>::get_reference_serial() {
  std::list<std::vector<T>* >& list = list_reducer.get_reference();
  int size = 0;
  for (typename std::list<std::vector<T>* >::iterator it=list.begin(); it != list.end(); ++it) {
    size += (*it)->size();
  }
  std::vector<T>* combined = new std::vector<T>();
  combined->resize(size);
  int total_added = 0;
  for (typename std::list<std::vector<T>* >::iterator it=list.begin(); it != list.end(); ++it) {
    cilk_for (int i = total_added; i < (*it)->size() + total_added; i++) {
      (*combined)[i] = (*(*it))[i-total_added];
    }
    total_added += (*it)->size();
  }
  return *combined;
}
*/

template <typename T>
std::vector<T>&
Vector_reducer<T>::get_reference()
{
  std::list<std::vector<T>* >& list = list_reducer.get_reference();
  if (list.size() == 1) {
    return *list.back();
  }
  int total_size = 0;
  int* starts = (int*) malloc(sizeof(int) * list.size());
  std::vector<T>** vector_list = (std::vector<T>**) malloc(sizeof(std::vector<T>*) * list.size());
  int index = 0;
  for (typename std::list<std::vector<T>* >::iterator it=list.begin(); it != list.end(); ++it) {
    starts[index] = total_size;
    total_size += (*it)->size();
    vector_list[index] = *it;
    index++;
  }
  std::vector<T>* combined = new std::vector<T>();
  combined->resize(total_size);

  cilk_for (int i = 0; i < list.size(); i++) {
    std::vector<T>* vector = vector_list[i];
    cilk_for (int j = starts[i]; j < vector->size() + starts[i]; j++) {
      (*combined)[j] = (*vector)[j - starts[i]];
    }
    delete vector;
  }

  list.clear();
  list.push_back(combined);

  return *combined;
}

// Unoptimized
template <typename T>
inline uint
Vector_reducer<T>::numElements() const
{
  return get_reference().size();
}

// Unoptimized
template <typename T>
inline bool
Vector_reducer<T>::isEmpty() const
{
  return get_reference().size() == 0;
}

// Unoptimized
template <typename T>
inline void
Vector_reducer<T>::clear()
{
  get_reference().clear();
}

#include "reducer_vector.cpp"

#endif
