// Copyright 2014

#ifndef GRAPH_PROPERTIES_H_
#define GRAPH_PROPERTIES_H_

#include "./scheduler.h"
#include "./engine.h"

// Programmer defined vertex and edge structures.
//   x,y,z required.
struct vdata{
  float p;
  float r;
  float last_r;
  uint32_t object_label;
  //float window_std;
  //float window_avg;
  uint16_t x;
  uint16_t y;
  uint16_t z;
//  uint8_t in_degree;
//  uint8_t out_degree;
  uint8_t pixel_value;
  uint8_t weak_average_value;
  uint8_t strong_average_value;
  uint8_t strong_min_value;
  uint8_t weak_min_value;
//  bool deleted;
  bool skeleton;
  explicit vdata(float p = 1) : p(p) { }
};

// This structure isn't really used since we're
// representing the edges implicitly.
struct edata {
  double weight;
  explicit edata(double weight = 1) : weight(weight) { }
};

// The scheduler and graph objects. Global for
// convenience.
static Scheduler* scheduler; Graph<vdata, edata>* graph;
static engine<vdata, edata>* e;
static bool terminated = false;

static uint8_t* frame_medians;
static uint8_t* frame_medians2;
static uint8_t* frame_medians3;
static uint8_t* frame_stds;

static int _height = 100;  // Number of images in the stack.
static int _rows = 1024;  // pixel rows in image in stack.
static int _cols = 1024;  // pixel cols in image in stack.

// Labels with fewer than this number of pixels are pruned.
//static int LABEL_MIN_PIXEL_COUNT = 40000;
//static int LABEL_MIN_PIXEL_COUNT = 5000;
static int LABEL_MIN_PIXEL_COUNT = 1;


#endif  // GRAPH_PROPERTIES_H_
