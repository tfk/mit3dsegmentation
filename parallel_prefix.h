// Copyright (c) 2013, Tim Kaler - MIT License

#ifndef PARALLEL_PREFIX_H
#define PARALLEL_PREFIX_H
#include <stdio.h>
#include <assert.h>
#include <cilk/cilk.h>
#define PREFIX_SUM_COARSENING 7919
#define PREFIX_SUM_SPAWN_COARSENING 7919


template<typename T>
void serial_prefix_sum_vectorized(T* summable, int size) {
  int i;
  for (i = 1; i < size-4; i+=4) {
    summable[i] += summable[i-1];
    summable[i+1] += summable[i];
    summable[i+2] += summable[i+1];
    summable[i+3] += summable[i+2];
  }
  for (; i < size; i++) {
    summable[i] += summable[i-1];
  }
}


// Helper functions. Needed to ensure that compiler uses right optimizations.
template<typename T>
T serial_sum_return(T* summable, int size) {
  T sum = 0;
  for (int i = 1; i < size+1; i++) {
    sum += summable[i-1];
  }
  return sum;
}

template<typename T>
T serial_sum_plus_return(T* summable, int size) {
  T sum = 0;

  if (size > 0) {
    sum += summable[0];
  }

  int i;
  for (i = 1; i < size - 1 - 4; i += 4) { // bug when size < 2
    sum += summable[i];
    summable[i] += summable[i-1];

    sum += summable[i+1];
    summable[i+1] += summable[i-1+1];

    sum += summable[i+2];
    summable[i+2] += summable[i-1+2];

    sum += summable[i+3];
    summable[i+3] += summable[i-1+3];
  }
  for (; i < size - 1; i++) { // bug when size < 2
    sum += summable[i];
    summable[i] += summable[i-1];
  }

  if (size > 2) {
    sum += summable[size-1];
  }
  return sum;
}

template<typename T>
void serial_sum(T* summable, int size) {
  // below improves single core performance significantly.
  int i;
  for (i = 1; i < size-1-4; i+=4) {
    summable[i] += summable[i-1];
    summable[i+1] += summable[i];
    summable[i+2] += summable[i+1];
    summable[i+3] += summable[i+2];
  }
  for (; i < size-1; i++) {
    summable[i] += summable[i-1];
  }
/*
  for (int i = 1; i < size-1; i++) {
    summable[i] += summable[i-1];
  }*/
}

template<typename T>
void prefix_sum_array_parallel_down (T* summable, int size, bool left) {
  int left_size = size / 2;
  int right_size = size-left_size;

  if (size > PREFIX_SUM_COARSENING) {
    if (size > PREFIX_SUM_SPAWN_COARSENING) {
      cilk_spawn prefix_sum_array_parallel_down(summable, left_size, left);
      prefix_sum_array_parallel_down(summable+left_size, right_size, false);
      cilk_sync;
    } else {
      prefix_sum_array_parallel_down(summable, left_size, left);
      prefix_sum_array_parallel_down(summable+left_size, right_size, false);
    }
    summable[size-1] += summable[left_size-1];
  } else if (size > 1) {
      #ifdef DEFER_SUM_COMP
      summable[size-1] = serial_sum_plus_return(summable,size);
      #endif
      #ifndef DEFER_SUM_COMP
      if (!left) {
        summable[size-1] = serial_sum_return(summable,size);
      } else {
        serial_sum(summable,size);
        summable[size-1] += summable[size-2];
      }
      #endif
  }
}

template<typename T>
void prefix_sum_array_parallel_up_inclusive (T* summable, int size,
  T num, bool left) {
  int left_size = size / 2;
  int right_size = size-left_size;

  if (size > PREFIX_SUM_COARSENING) {
    //assert(left_size > 0);
    T temp = num - summable[left_size-1];
    T mid = summable[left_size-1];
    summable[left_size-1] = summable[size-1] - temp;
    if (size > PREFIX_SUM_SPAWN_COARSENING) {
      cilk_spawn prefix_sum_array_parallel_up_inclusive(summable, left_size, mid, left);
    } else {
      prefix_sum_array_parallel_up_inclusive(summable, left_size, mid, left);
    }
    prefix_sum_array_parallel_up_inclusive(summable+left_size, right_size, temp, false);
    cilk_sync;
  }
  #ifndef DEFER_SUM_COMP
  else if (size > 1) {
    if (!left) {
      summable[0] += summable[-1];
      serial_sum(summable, size);
    }
  }
  #endif
}

template<typename T>
void prefix_sum_array (T* summable, int size) {
  prefix_sum_array_parallel_down(summable, size, true);
  prefix_sum_array_parallel_up_inclusive(summable, size, summable[size-1],
      true);
}


// For deferred sum computation.
int read_value(int* summable, int index) {
  if (index % PREFIX_SUM_COARSENING == PREFIX_SUM_COARSENING-1 || index < PREFIX_SUM_COARSENING) {
    return summable[index];
  } else {
    int offset = ((index) / PREFIX_SUM_COARSENING) * PREFIX_SUM_COARSENING - 1;
    return summable[index] + summable[offset];
  }
}

// Type P requires field: prefix_sum, and size
template <typename P>
void prefix_sum (std::vector<P>& summable) {
  int* sum_array = (int*) malloc(summable.size()*sizeof(int));
  {
  cilk_for (int i = 0; i < summable.size(); i++) {
    sum_array[i] = summable[i].size;
  }
  }
  prefix_sum_array(sum_array, summable.size());
  {
  cilk_for (int i = 0; i < summable.size(); i++) {
    summable[i].prefix_sum = sum_array[i];
  }
  }
}


// Not used, but here as a usage example.
template<typename S>
void integer_sort_mask(S* sortables, S* sorted, int max_key, int total_objects, int key_mask, int shift) {
  //assert(total_objects == sortables.size() && total_objects == sorted.size());
  int N = total_objects;
  int k = max_key*256;
  int num_blocks = N / k + 1;
  int* histograms = (int*) calloc(sizeof(int),num_blocks*k);
  int histogram_size = num_blocks*k;

  double sort2_start = tfk_get_time();
  {
  cilk_for (int block = 0; block < num_blocks; block++) {
    int lower_limit = block*k;
    int upper_limit = (block+1)*k;

    if (upper_limit > N) upper_limit = N;

    // For each block increment the histogram.
    int i;
    for (i = lower_limit; i < upper_limit; i++) {
      int color_offset = ((sortables[i].reducer_id & key_mask) >> shift)*num_blocks;
      int block_offset = block;
      //assert((sortables[i].reducer_id&key_mask)>>shift >= 0 && (sortables[i].reducer_id&key_mask)>>shift < k);
      sortables[i].index = histograms[color_offset+block_offset]++;
    }
  }
  }
  double sort2_end = tfk_get_time();
  printf("time spent near beginning of sort function %f\n", sort2_end - sort2_start);

  double prefix_sum_start = tfk_get_time();
  prefix_sum_array(histograms, num_blocks*k);
  double prefix_sum_end = tfk_get_time();
  printf("time spent doing prefix sum %f\n", prefix_sum_end - prefix_sum_start);
/*
  color_counts.resize(k);

  cilk_for (int c = 0; c < k; c++) {
    if (c != 0) {
      assert(c*num_blocks-1 >= 0);
      if ((c+1)*num_blocks < histogram_size) {
        color_counts[c] =
            histograms[(c+1)*num_blocks - 1] - histograms[c*num_blocks-1];
      } else {
        color_counts[c] = total_objects - histograms[c*num_blocks-1];
      }
    } else {
      color_counts[c] = histograms[(c+1)*num_blocks - 1];
    }
    assert (color_counts[c] >= 0);
  }
*/

  double sort_start = tfk_get_time();
  {
  cilk_for (int block = 0; block < num_blocks; block++) {
    cilk_for (int j = 0; j < k; j++) {
      int i = block*k + j;
      if (i < N) {
        int offset = 0;
        int x = ((sortables[i].reducer_id&key_mask) >> shift)*num_blocks + block-1;

        if (x >= 0) offset = histograms[x];

        int index = sortables[i].index + offset;
        sorted[index] = sortables[i];
      }
    }
  }
  }
  free(histograms);
  double sort_end = tfk_get_time();
  printf("time spent near end of sort routine %f\n", sort_end - sort_start);
}


// Not used, but here as a usage example.
template<typename S>
void integer_sort(S* sortables, S* sorted, int max_key, int total_objects, int* color_counts) {

  if (max_key > total_objects || true) {
    int log_key = floor(log2(max_key));
    int mask1 = (1 << (log_key/2)) - 1;
    int mask2 = ~mask1;
    int max_key1 = 1<<(log_key/2);
    int max_key2 = 1<<(log_key/2+1);
    S* sorted_temp = (S*) calloc(sizeof(S), total_objects);
    integer_sort_mask(sortables, sorted_temp, max_key1, total_objects, mask1, 0);
    integer_sort_mask(sorted_temp, sorted, max_key2, total_objects, mask2, log_key/2);
    //color_counts.resize(max_key+1);
    free(sorted_temp);
    {
    cilk_for (int i = 0; i < total_objects; i++) {
      int color = sorted[i].reducer_id;
      __sync_fetch_and_add(&color_counts[color], 1);
      //color_counts[color]++;
    }
    }
    return;
  }

  int N = total_objects;
  int k = (max_key+1);
  int num_blocks = N / k + 1;
  int* histograms = (int*) calloc(sizeof(int),num_blocks*k);
  int histogram_size = num_blocks*k;
  {
  cilk_for (int block = 0; block < num_blocks; block++) {
    int lower_limit = block*k;
    int upper_limit = (block+1)*k;

    if (upper_limit > N) upper_limit = N;

    // For each block increment the histogram.
    int i;
    for (i = lower_limit; i < upper_limit; i++) {
      int color_offset = sortables[i].reducer_id*num_blocks;
      int block_offset = block;
      assert(sortables[i].reducer_id >= 0 && sortables[i].reducer_id < k);
      sortables[i].index = histograms[color_offset+block_offset]++;
    }
  }
  }
  prefix_sum_array(histograms, num_blocks*k);
  {
  //color_counts.resize(k);
  cilk_for (int c = 0; c < k; c++) {
    if (c != 0) {
      assert(c*num_blocks-1 >= 0);
      if ((c+1)*num_blocks < histogram_size) {
        color_counts[c] =
            histograms[(c+1)*num_blocks - 1] - histograms[c*num_blocks-1];
      } else {
        color_counts[c] = total_objects - histograms[c*num_blocks-1];
      }
    } else {
      color_counts[c] = histograms[(c+1)*num_blocks - 1];
    }
    assert (color_counts[c] >= 0);
  }
  }
  {
  cilk_for (int block = 0; block < num_blocks; block++) {
    for (int j = 0; j < k; j++) {
      int i = block*k + j;
      if (i < N) {
        int offset = 0;
        int x = sortables[i].reducer_id*num_blocks + block-1;

        if (x >= 0) offset = histograms[x];

        int index = sortables[i].index + offset;
        sorted[index] = sortables[i];
      }
    }
  }
  }
  free(histograms);
}

#endif
