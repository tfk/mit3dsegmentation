#include <sys/time.h>
#include <sys/resource.h>
#include <math.h>

int slice_standard_dev(int* histogram, int hist_length, int size) {
	int sum = 0;
	double std_accum = 0.0;
	double avg;
	for (int i = 0; i < hist_length; i++){
		sum += i * histogram[i];
	}
	avg = (double) sum / size;
	for (int i = 0; i < hist_length; i++){
		std_accum += histogram[i] * (i - avg) * (i - avg);
	}
	return (int)sqrt(std_accum / (size - 1));
}