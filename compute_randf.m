%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SNEMI3D challenge: 3D segmentation of neurites in EM images
% 
% Script to calculate the segmentation error between some 3D 
% original labels and their corresponding proposed labels. 
% 
% The evaluation metric is:
%  - Rand error: 1 - F-score of adapted Rand index
% 
% author: Ignacio Arganda-Carreras (iarganda@mit.edu)
% More information at http://brainiac.mit.edu/SNEMI3D
%
% This script released under the terms of the General Public 
% License in its latest edition.
%
% Input: 
%       segA - ground truth (16-bit labels, 0 = background)
%       segB - proposed labels (16-bit labels, 0 = background)
% Output:
%       re - adapated Rand error (1.0 - F-score of adapted Rand index)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [re] = compute_randf()

I=[];

for i=0:99
  I = cat(3,I,imread(strcat('/scratch/3dsegstack3/output-stack-label1-',num2str(i,'%d'),'.tif')));
end

%outputFileName = '/scratch/3dsegstack3/unified-labels.tif'
%for K=1:length(I(1,1,:))
%  imwrite(I(:,:,K), outputFileName, 'WriteMode', 'append');
%end

C=[];
for i=1:100
  C = cat(3,C,imread('/scratch/rhoana/train-labels.tif',i));
end

%re = size(C)
%segB = imread('output_labels.bak4/labels_00000.tif');

re = SNEMI3D_metrics(C,I);
re
