// Copyright 2014

#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cilk/cilk.h>
#include <cilk/reducer.h>
#include <cilk/reducer_opadd.h>

#include <string>
#include <set>

#include "../tfk_utils.h"
#include "../parallel_utils.h"
#include "../vector_reducer.h"
#include "../graph_properties.h"  // Includes vdata/edata and height/cols/rows.
#include "../imageGraphIO.h"  // Includes vdata/edata and height/cols/rows.

#include "../Graph.h"
#include "../engine.h"
#include "../Graph.cpp"

// Path to ith input image is:
// INPUT_IMAGE_DIR + "/" + INPUT_IMAGE_FILENAME_PREFIX + "i" +
//       INPUT_IMAGE_FILENAME_EXT
static std::string INPUT_IMAGE_DIR =
    "/scratch/segmentation_directory/grayscale_input1";
static std::string INPUT_IMAGE_FILENAME_PREFIX = "input-image";
static std::string INPUT_IMAGE_FILENAME_EXT = ".tif";

static std::string LABELED_IMAGE_DIR =
    "/scratch/segmentation_directory/labels_input1";
static std::string LABELED_IMAGE_FILENAME_PREFIX =
    "input-labels-";
static std::string LABELED_IMAGE_FILENAME_EXT = ".tif";

static int OPENCV_LABEL_TYPE = CV_16U;
typedef uint16_t LABEL_TYPE;

// Path format for output files
//   Will create images with .jpg extension suitable for human viewing,
//   and .tif extension for algorithmic comparison with ground truth.
static std::string OUTPUT_IMAGE_DIR = "./vesicle_density_stack";
static std::string OUTPUT_IMAGE_FILENAME_PREFIX = "output-stack-image";
static std::string OUTPUT_IMAGE_FILENAME_EXT = ".png";

static std::string OUTPUT_HEATMAP_DIR = "./vesicle_density_stack";
static std::string OUTPUT_HEATMAP_FILENAME_PREFIX = "output-stack-heatmap";
static std::string OUTPUT_HEATMAP_FILENAME_EXT = ".tif";

static int OPENCV_DENSITY_TYPE = CV_32F;
typedef float DENSITY_TYPE;


// Input: Grayscale image stack + Object-Labeled image stack
// Output (currently): Vescicle Density pixel->floating-point
int main(int argc, char **argv) {
  int rows = _rows;
  int cols = _cols;
  int height = _height;

  // arg 1: rand seed
  // arg 2: grayscale image directory
  // arg 3: labeled image directory
  if (argc > 1) {
    std::string argstring(argv[1]);
    int seed_arg = StringToNumber<int>(argstring);
    srand(seed_arg);
  } else {
    srand(0);
  }

  // If input directories are specified, then overwrite defaults.
  if (argc > 3) {
    std::string grayscale_image_directory(argv[2]);
    std::string labeled_image_directory(argv[3]);
    INPUT_IMAGE_DIR = grayscale_image_directory;
    LABELED_IMAGE_DIR = labeled_image_directory;
  }

  LABEL_TYPE** labels_array =
     static_cast<LABEL_TYPE**>(malloc(sizeof(LABEL_TYPE*)*height));
  uint8_t** input_image_stack =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));

  // load the image stack
  for (int i = 0; i < height; i++) {
    // Load ith level of grayscale stack.
    cv::Mat im;
    std::string grayscale_path = INPUT_IMAGE_DIR + "/" +
        INPUT_IMAGE_FILENAME_PREFIX + NumberToString(i) +
        INPUT_IMAGE_FILENAME_EXT;
    printf("Grayscale path is %s\n", grayscale_path.c_str());
    im = cv::imread(grayscale_path.c_str(), 0);
    im.convertTo(im, CV_8U);
    printf("Success dims %d %d\n", im.dims, i);
    input_image_stack[i] = Mat_to_Row_Major<uint8_t>(im);

    // Load ith level of label stack.
    std::string labels_path = LABELED_IMAGE_DIR + "/" +
        LABELED_IMAGE_FILENAME_PREFIX + NumberToString(i) +
        LABELED_IMAGE_FILENAME_EXT;
    cv::Mat im2;
    im2 = cv::imread(labels_path.c_str(), CV_LOAD_IMAGE_UNCHANGED);
    printf("Labels path is %s\n", labels_path.c_str());
    im2.convertTo(im2, OPENCV_LABEL_TYPE);
    printf("Success dims %d %d\n", im2.dims, i);
    labels_array[i] = Mat_to_Row_Major<LABEL_TYPE>(im2, OPENCV_LABEL_TYPE);
  }

  // Create vesicle density array.
  // Algorithm should populate it with numbers in range [0,1]
  DENSITY_TYPE** vdensities_array = reinterpret_cast<DENSITY_TYPE**>
      (calloc(height, sizeof(DENSITY_TYPE*)));
  for (int z = 0; z < height; z++) {
    vdensities_array[z] = reinterpret_cast<DENSITY_TYPE*>
        (calloc(rows*cols, sizeof(DENSITY_TYPE)));
  }

  // Skip this for now
  // graph = row_major_to_graph(input_image_stack, rows, cols, height);

  DENSITY_TYPE** input_image_stack_float =
      reinterpret_cast<DENSITY_TYPE**>(malloc(sizeof(DENSITY_TYPE*)*height));
  cilk_for (int z = 0; z < height; z++) {
    input_image_stack_float[z] =
        reinterpret_cast<DENSITY_TYPE*>(malloc(sizeof(DENSITY_TYPE)*rows*cols));
    cilk_for (int xy = 0; xy < rows*cols; xy++) {
      input_image_stack_float[z][xy] =
          static_cast<DENSITY_TYPE>(input_image_stack[z][xy]);
    }
  }

  // Find max label.
  int max_label = 0;
  for (int z = 0; z < height; z++) {
    for (int xy = 0; xy < rows*cols; xy++) {
      LABEL_TYPE label = labels_array[z][xy];
      if (max_label < label) max_label = label;
    }
  }
  printf("The max label is %d\n", max_label);

  // Populate with fake densities for testing.
  cilk_for (int z = 0; z < height; z++) {
    cilk_for (int xy = 0; xy < rows*cols; xy++) {
      vdensities_array[z][xy] = static_cast<DENSITY_TYPE>(xy)/(1024*1024);
    }
  }

  // output density map
  for (int z = 0; z < height; z++) {
    std::string path = OUTPUT_IMAGE_DIR + "/" + OUTPUT_IMAGE_FILENAME_PREFIX +
        NumberToString(z) + OUTPUT_IMAGE_FILENAME_EXT;
    std::string path_heatmap = OUTPUT_IMAGE_DIR + "/" +
        OUTPUT_HEATMAP_FILENAME_PREFIX + NumberToString(z) +
        OUTPUT_HEATMAP_FILENAME_EXT;

    cv::Mat slice = cv::Mat(rows, cols, CV_32F, vdensities_array[z]);
    cv::Mat original = cv::Mat(rows, cols, CV_32F, input_image_stack_float[z]);

    std::vector<cv::Mat> channels;
    channels.push_back(original);
    channels.push_back(original);
    channels.push_back((slice)*255+original);
    cv::Mat color;
    cv::merge(channels, color);

    imwrite(path.c_str(), color);
    imwrite(path_heatmap.c_str(), slice*255);
  }

  return 0;
}

