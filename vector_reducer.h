// Copyright (c) 2013, Tim Kaler - MIT License

#ifndef REDUCER_VECTOR_H
#define REDUCER_VECTOR_H

#include <stdlib.h>
#include <assert.h>
#include <vector>
#include <list>
#include <cilk/cilk.h>
#include <cilk/reducer.h>
#include <cilk/reducer_list.h>
#include <cilk/cilk_api.h>

template <typename T>
class Vector_reducer
{
private:
  //cilk::reducer_list_append<std::vector<T>* > list_reducer;
public:
  Vector_reducer();
  ~Vector_reducer();
  void insert(T);
  std::vector<T> &get_reference();
  std::vector<T> &get_reference_serial();
  inline bool isEmpty() const;
  inline void clear();
  std::vector<T>** vectors;
  std::vector<T> combined;
  bool iscombined;
};

template <typename T>
Vector_reducer<T>::Vector_reducer(){
  vectors = (std::vector<T>**)malloc(sizeof(std::vector<T>*)*(__cilkrts_get_nworkers()+1));
  for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
    vectors[i] =  new std::vector<T>();
  }
  iscombined = false;
}

template <typename T>
Vector_reducer<T>::~Vector_reducer(){
  for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
    vectors[i]->clear();
    delete (vectors[i]);
  }
  free(vectors);
}

template <typename T>
void
Vector_reducer<T>::insert(T el)
{
  int wid = __cilkrts_get_worker_number();
  vectors[wid]->push_back(el);
}

template <typename T>
std::vector<T>&
Vector_reducer<T>::get_reference()
{
  if (iscombined) return combined;
  combined.clear();
  for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
    int offset = combined.size();
    combined.resize(combined.size() + vectors[i]->size());
    {
    cilk_for(int j = 0; j < vectors[i]->size(); j++) {
      combined[j+offset] = (*(vectors[i]))[j];
    }
    }
  }
  iscombined = true;
  return combined;
}
/*
// Unoptimized
template <typename T>
inline uint32_t
Vector_reducer<T>::numElements() const
{
  return get_reference().size();
}
*/
// Unoptimized
template <typename T>
inline bool
Vector_reducer<T>::isEmpty() const
{
  return get_reference().size() == 0;
}

// Unoptimized
template <typename T>
inline void
Vector_reducer<T>::clear()
{
  get_reference().clear();
  for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
    vectors[i]->clear();
  }
  iscombined = false;
}

#endif
