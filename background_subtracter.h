#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <time.h>

// Macros
// #define MAX(x, y) (((x) > (y)) ? (x) : (y))
// #define MIN(x, y) (((x) < (y)) ? (x) : (y))


// constants
#define MAXIMUM 0
#define MEAN 1
#define X_DIRECTION 0
#define Y_DIRECTION 1
#define DIAG_1A 2
#define DIAG_1B 3
#define DIAG_2A 4
#define DIAG_2B 5



// function declaration
void background_subtract(uint8_t* image, int rows, int cols, int radius, bool do_presmooth, bool correct_corners, bool light_bg);
float* sliding_paraboloid(uint8_t* image, int rows, int cols, int radius, bool do_presmooth, bool correct_corners, bool invert);
void filter_1D(float* bg, int rows, int cols, int dir, float coeff2, float* cache, int* next_point);
static float* line_slide_parabola(float* bg, int start, int inc, int length, float coeff2, 
									float* cache, int* next_point, float* corrected_edges);
void correct_corners (float* bg, int rows, int cols, float coeff2, float* cache, int* next_point);
float filter_3x3(float* bg, int rows, int cols, int type);
float filter_3(float* bg, int length, int pixel0, int inc, int type);
float average3 (float x, float y, float z);
// void print_image(uint8_t* image, int rows, int cols);




// int main (){
// 	uint8_t* image = (uint8_t*)malloc(400*sizeof(uint8_t));
// 	srand(time(NULL));
// 	for (int i = 0; i < 400; i++) 
// 		image[i] = rand() % 255;
// 	print_image(image, 20, 20);
// 	background_subtract(image, 20, 20, 50, true, true, true);
// 	printf ("Background removed: \n");
// 	print_image(image, 20, 20);
// 	return 0;
// }



void print_image(uint8_t* image, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) 
			printf("%6u", image[i * cols + j]);
		printf("\n");
	}
}


void background_subtract(uint8_t* image, int rows, int cols, int radius, bool do_presmooth, bool correct_corners, bool light_bg) {
	bool invert = light_bg;
	float* bg_pixels = sliding_paraboloid(image, rows, cols, radius, do_presmooth, correct_corners, invert);

	// subtract background
	// for (int p = 0; p < rows * cols; p++){

	// 	// assert(bg_pixel <= image[p])
	// 	float image_p = ((float)image[p]) - bg_pixels[p];
		
	// 	if (bg_pixels[p] > image[p])
	// 		printf("CHECK1");
	// 	if (image_p < 0.0)
	// 		image_p = 0.0;
	// 	else if (image_p > 255.0)
	// 		image_p = 255.0;
    	
 //    	image[p] = (uint8_t)image_p;
	// }

  	float offset = invert ? 255.5 : 0.5;  //includes 0.5 for rounding when converting float to byte

    for (int p = 0; p < rows*cols; p++) {
        float value = (image[p] & 0xff) - bg_pixels[p] + offset;
        
        if (value < 0.0) 
        	value = 0.0;
        if (value > 255.0) 
        	value = 255.0;

        image[p] = (uint8_t)(value);
    }

  	free(bg_pixels);
	return;
}

float* sliding_paraboloid(uint8_t* image, int rows, int cols, int radius, bool do_presmooth, bool correct_corn, bool invert) {
	float* bg = (float*)malloc(rows * cols * sizeof(float));
	float cache [MAX(rows, cols)];
	int next_point [MAX(rows, cols)];
	float coeff2 = 0.5 / radius; // 2nd-order coefficient of the polynomial approximating the ball
	float coeff2_diag = 1.0 / radius;

    for (int i = 0; i < rows*cols; i++) {
    	bg[i] = 1.0 * image[i];
    	if (invert)
    		bg[i] = -bg[i];
    }

	float shift_by = 0.0;
	
	if (do_presmooth) {
		shift_by = (float) filter_3x3(bg, rows, cols, MAXIMUM);
		filter_3x3(bg, rows, cols, MEAN);
	}

	if (correct_corn)
		correct_corners(bg, rows, cols, coeff2, cache, next_point);

	filter_1D (bg, rows, cols, X_DIRECTION, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, Y_DIRECTION, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, X_DIRECTION, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_1A, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_1B, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_2A, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_2B, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_1A, coeff2, cache, next_point);
	filter_1D (bg, rows, cols, DIAG_1B, coeff2, cache, next_point);	

    if (invert)
        for (int i = 0; i < rows * cols; i++)
            bg[i] = -(bg[i] - shift_by);
	if (do_presmooth)
		for (int i = 0; i < rows * cols; i++)
			bg[i] -= shift_by;

	return bg;
}



void filter_1D(float* bg, int rows, int cols, int dir, float coeff2, float* cache, int* next_point) {
	int start_line = 0;
	int num_lines = 0;
	int line_increment = 0;
	int point_increment = 0;
	int length = 0; // length of line

	switch (dir) {
		case X_DIRECTION:
			num_lines = rows;
			line_increment = cols;
			point_increment = 1;
			length = cols;
			break;
		case Y_DIRECTION:
			num_lines = cols;
			line_increment = 1;
			point_increment = cols;
			length = rows;
			break;
		case DIAG_1A:	
			num_lines = cols - 2;
			line_increment = 1;
			point_increment = cols + 1;
			break;
		case DIAG_1B:	
			start_line = 1;
			num_lines = rows - 2;
			line_increment = cols;
			point_increment = cols + 1;
			break;
		case DIAG_2A:	
			start_line = 2;
			num_lines = cols;
			line_increment = 1;
			point_increment = cols - 1;
			break;
		case DIAG_2B:	
			start_line = 0;
			num_lines = rows - 2;
			line_increment = cols;
			point_increment = cols - 1;
			break;
	}

	for (int i = start_line; i < num_lines; i++) {
		int start_pixel = i * line_increment;
		if (dir == DIAG_2B)
			start_pixel += cols - 1;
		switch (dir) {
			case DIAG_1A:
				length = MIN(rows, cols - i);
				break;
			case DIAG_1B:
				length = MIN(cols, rows - i);
				break;
			case DIAG_2A:
				length = MIN(rows, i + 1);
				break;
			case DIAG_2B:
				length = MIN(cols, rows - i);
				break;
		}
		line_slide_parabola(bg, start_pixel, point_increment, length, coeff2, cache, next_point, NULL);
	}
}



static float* line_slide_parabola(float* bg, int start, int inc, int length, float coeff2, 
									float* cache, int* next_point, float* corrected_edges) {
	float min_val = FLT_MAX;
	int last_point = 0;
	int first_corner = length - 1;             // the first point except the edge that is touched
    int last_corner = 0;                     // the last point except the edge that is touched
    float v_prev_1 = 0.0;
    float v_prev_2 = 0.0;
    float curvature_test = 1.999 * coeff2; 

    int p = start;

    for (int i = 0; i < length; i++, p += inc) {
    	int val = bg[p];
    	cache[i] = val;
    	if (val < min_val)
    		min_val = val;
    	if (i >= 2 && v_prev_1 + v_prev_1 - v_prev_2 - val < curvature_test) {
    		next_point[last_point] = i - 1;
    		last_point = i - 1;
    	}
    	v_prev_2 = v_prev_1;
    	v_prev_1 = val;
    }
    
    next_point[last_point] = length - 1;
    next_point[length - 1] = INT_MAX;

    int point_1 = 0;                             // point_1 and point_2 will be the two points where the parabola touches
    while (point_1 < length - 1) {
        float v1 = cache[point_1];
        float min_slope = FLT_MAX;
        int point_2 = 0;                        
        int search_to = length;
        int recalculate_limit_now = 0;        // when 0, limits for searching will be recalculated
        // find the second point where the parabola through point point_1, v1 touches:
        for (int j = next_point[point_1]; j < search_to; j = next_point[j], recalculate_limit_now++) {
            float v2 = cache[j];
            float slope = (float)(v2 - v1)/(j - point_1) + coeff2 * (j - point_1);
            if (slope < min_slope) {
                min_slope = slope;
                point_2 = j;
                recalculate_limit_now = -3;
            }
            if (recalculate_limit_now == 0) {   // time-consuming recalculation of search limit: wait a bit after slope is updated
                float b = 0.5 * min_slope / coeff2;
                int max_search = point_1 + (int)(b + sqrt(b * b + (v1 - min_val) / coeff2) + 1); //(numeric overflow may make this negative)
                if (max_search < search_to && max_search > 0) search_to = max_search;
            }
        }

        if (point_1 == 0) 
        	first_corner = point_2;
        if (point_2 == length - 1) 
        	last_corner = point_1;

        // interpolate between the two points where the parabola touches:

    	// printf(" %u %u\n", point_1, point_2);
        int p = start + point_1 + 1 + inc;
        for (int j = point_1 + 1; j < point_2; j++, p += inc) {
            bg[p] = v1 + (j - point_1) * (min_slope - (j - point_1) * coeff2);
        }
        point_1 = point_2;                            // continue from this new point
    }

    /* Now calculate estimated edge values without an edge particle, allowing for vignetting
     * described as a 6th-order polynomial: */
    if (corrected_edges != NULL) {
        if (4 * first_corner >= length) 
        	first_corner = 0; // edge particles must be < 1/4 image size
        if (4* (length - 1 - last_corner) >= length)
        	last_corner = length - 1;
        float v1 = cache[first_corner];
        float v2 = cache[last_corner];
        float slope = (float)(v2 - v1)/(last_corner - first_corner); // of the line through the two outermost non-edge touching points
        float value0 = v1 - slope * first_corner;        // offset of this line
        float coeff6 = 0.0;                               // coefficient of 6th order polynomial
        float mid = 0.5 * (last_corner + first_corner);
        for (int i = (length + 2)/3; i <= (2*length)/3; i++) {// compare with mid-image pixels to detect vignetting
            float dx = (i - mid) * 2.0/(last_corner - first_corner);
            float poly6 = pow(dx, 6.0) - 1.0;       // the 6th order polynomial, zero at firstCorner and lastCorner
            if (cache[i] < value0 + slope * i + coeff6 * poly6) {
                coeff6 = -(value0 + slope * i - cache[i])/poly6;
            }
        }
        float dx = (first_corner - mid) * 2.0/(last_corner - first_corner);
        corrected_edges[0] = value0 + coeff6 * (pow(dx, 6.0) - 1.0) + coeff2 * first_corner * first_corner;
        dx = (last_corner - mid) * 2.0/(last_corner - first_corner);
        corrected_edges[1] = value0 + (length - 1) * slope + coeff6 * (pow(dx, 6.0) - 1.0) + coeff2 * (length - 1 - last_corner) * (length - 1 - last_corner);
    }

    return corrected_edges;
}


void correct_corners (float* bg, int rows, int cols, float coeff2, float* cache, int* next_point) {
	float corners [4];
	float* corrected_edges = (float*)malloc(2 * sizeof(float));
	corrected_edges = line_slide_parabola(bg, 0, 1, cols, coeff2, cache, next_point, corrected_edges);
	corners[0] = corrected_edges[0];
	corners[1] = corrected_edges[1];
	corrected_edges = line_slide_parabola(bg, (rows - 1) * cols, 1, cols, coeff2, cache, next_point, corrected_edges);
	corners[2] = corrected_edges[0];
	corners[3] = corrected_edges[1];
	corrected_edges = line_slide_parabola(bg, 0, cols, rows, coeff2, cache, next_point, corrected_edges);
	corners[0] += corrected_edges[0];
	corners[2] += corrected_edges[1];
	corrected_edges = line_slide_parabola(bg, cols - 1, cols, rows, coeff2, cache, next_point, corrected_edges);
	corners[1] += corrected_edges[0];
	corners[3] += corrected_edges[1];
	int diag_len = MIN(rows, cols);
	float coeff2_diag = 2*coeff2;
	corrected_edges = line_slide_parabola(bg, 0, 1 + cols, diag_len, coeff2_diag, cache, next_point, corrected_edges);
	corners[0] += corrected_edges[0];
	corrected_edges = line_slide_parabola(bg, cols - 1, cols - 1, diag_len, coeff2_diag, cache, next_point, corrected_edges);
	corners[1] += corrected_edges[0];
	corrected_edges = line_slide_parabola(bg, (rows - 1) * cols, 1 - cols, diag_len, coeff2_diag, cache, next_point, corrected_edges);
	corners[2] += corrected_edges[0];
	corrected_edges = line_slide_parabola(bg, rows * cols - 1, -1 - cols, diag_len, coeff2_diag, cache, next_point, corrected_edges);
	corners[3] += corrected_edges[0];

	if (bg[0] > corners[0]/3)
		bg[0] = (int)(corners[0]/3);
	if (bg[cols - 1] > corners[1]/3)
		bg[cols - 1] = (int)(corners[1]/3);
	if (bg[(rows - 1) * cols] > corners[2]/3)
		bg[(rows - 1) * cols] = (int)(corners[2]/3);
	if (bg[rows * cols - 1] > corners[3]/3)
		bg[rows * cols - 1] = (int)(corners[3]/3);

  free(corrected_edges);
}


float filter_3x3(float* bg, int rows, int cols, int type) {
	double shift_by = 0;
	for (int y = 0; y < rows; y++)
		shift_by += filter_3(bg, cols, y * cols, 1, type);
	for (int x = 0; x < cols; x++)
		shift_by += filter_3(bg, rows, x, cols, type);
	return shift_by/(rows * cols);
}


float filter_3(float* bg, int length, int pixel0, int inc, int type) {
	double shift_by = 0;
	float v3 = bg[pixel0];
	float v2 = v3;
	float v1;

	int p = pixel0;
	for (int i = 0; i < length; i++, p += inc) {
		v1 = v2; 
		v2 = v3;
		if (i < length - 1) {
			v3 = bg[p + inc];
		}
		if (type == MAXIMUM) {
			int max = v1 > v3 ? v1 : v3;
			if (v2 > max) {
				max = v2;
			}
			shift_by += max - v2;
			bg[p] = max;
		} else {
			bg[p] = average3(v1, v2, v3);
		}
	}

	return shift_by;
}


float average3 (float x, float y, float z) {
	return (x + y + z) / 3;
}