// Copyright 2014

#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>



#include <cilk/cilk.h>
#include <cilk/reducer.h>
#include <cilk/reducer_opadd.h>

#include <opencv2/core/core.hpp>
//#include <opencv2/highgui.hpp>
#include <opencv/highgui.h>
#include <string>

#include "./tfk_utils.h"
#include "./quan_utils.h"
#include "./parallel_utils.h"
#include "./vector_reducer.h"
#include "./Graph.h"
#include "./engine.h"
#include "./graph_properties.h"  // Includes vdata/edata and height/cols/rows.
#include "./segmentation.cpp"
#include "./imageGraphIO.h"
#include "./background_subtracter.h"

#include "./Graph.cpp"
#include "./scheduler.cpp"
#include "./engine.cpp"


// Path to ith input image is:
// INPUT_IMAGE_DIR + "/" + INPUT_IMAGE_FILENAME_PREFIX + "i" +
//       INPUT_IMAGE_FILENAME_EXT
static std::string INPUT_IMAGE_DIR =
    "/scratch/segmentation_directory/grayscale_input1_preproc";
   // "/scratch/segmentation_directory/grayscale_input1";
static std::string INPUT_IMAGE_FILENAME_PREFIX = "input-image";
static std::string INPUT_IMAGE_FILENAME_EXT = ".tif";

// Path format for output files
//   Will create images with .jpg extension suitable for human viewing,
//   and .tif extension for algorithmic comparison with ground truth.
static std::string OUTPUT_IMAGE_DIR = "/scratch/3dsegstack3";
static std::string OUTPUT_IMAGE_FILENAME_PREFIX = "output-stack-label";

int main(int argc, char **argv) {
  //printf("size of vdata %d\n", sizeof(vdata));
  //return 0;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  if (argc > 1) {
    std::string argstring(argv[1]);
    int seed_arg = StringToNumber<int>(argstring);
    srand(seed_arg);
  } else {
    srand(0);
  }
  printf("hello there\n");
  Vector_reducer<int> v;
  uint32_t** labels_array =
     static_cast<uint32_t**>(malloc(sizeof(uint32_t*)*height));
  uint8_t** input_image_stack =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc1 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc2 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc3 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint16_t** input_image_stack_labels =
      static_cast<uint16_t**>(calloc(_height, sizeof(uint16_t*)));

  for (int z = 0; z < height; z++) {
    labels_array[z] =
        static_cast<uint32_t*>(calloc(_rows*_cols, sizeof(uint32_t)));
  }

  // load the image stack
  for (int i = 0; i < height; i++) {
    cv::Mat im;
    std::string path = INPUT_IMAGE_DIR + "/" + INPUT_IMAGE_FILENAME_PREFIX +
        NumberToString(i) + INPUT_IMAGE_FILENAME_EXT;
    printf("%s \n", path.c_str());
    im = cv::imread(path.c_str(), 0);
    im.convertTo(im, CV_8U);
    printf("Success dims %d %d\n", im.dims, i);
    printf("Converting opencv mat to row major array\n");
    input_image_stack[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc1[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc2[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc3[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stack_labels[i] =
        static_cast<uint16_t*>(calloc(_rows * _cols, sizeof(uint16_t)));
  }
  printf("Done\n");

  // Allocate and obtain the frame medians and stds.
  frame_medians = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  frame_medians2 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  frame_medians3 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  frame_stds = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));

  // frame_threshold_1 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  // frame_threshold_2 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  // frame_threshold_3 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));
  // frame_threshold_4 = static_cast<uint8_t*>(malloc(sizeof(uint8_t)*_height));

  // uint8_t th1[100] = {71,84,47,21,85,23,50,15,25,35,80,20,40,32,53,15,21,65,26,20,50,43,35,32,35,82,20,70,
  //                   14,43,30,90,100,38,50,110,50,45,30,40,32,80,50,35,20,50,61,38,115,70,35,25,30,50,20,
  //                   70,54,65,60,65,50,118,35,58,60,75,50,45,32,30,50,58,67,50,70,43,60,43,32,45,53,51,54,
  //                   60,38,30,70,67,80,30,38,40,38,60,36,42,42,21,22, 25};

  // uint8_t th2[100] = {110,121,95,83,133,90,103,65,75,92,132,78,102,80,97,70,76,110,95,65,96,95,88,92,96,146,
  //                   78,127,87,100,98,140,157,90,110,160,100,95,89,90,88,133,100,84,70,116,107,82,157,128,83,
  //                   80,83,98,86,118,104,121,110,112,102,162,81,102,96,120,99,93,74,80,105,95,110,93,111,84,95,
  //                   83,68,80,100,97,96,100,82,67,120,112,122,72,82,82,80,100,80,88,88,67,68,75};

  // uint8_t th3[100] = {134,150,118,116,160,115,140,99,100,128,160,100,128,115,118,93,100,132,127,95,122,117,120,120,
  //                   127,174,104,162,111,130,127,166,184,113,132,190,130,116,115,113,117,157,125,114,100,144,139,110,195,150,
  //                   104,104,113,130,114,145,125,146,140,140,125,193,108,124,118,145,121,116,98,103,127,127,135,120,140,110,
  //                   120,107,97,110,132,130,125,130,109,91,150,140,148,102,110,105,107,127,100,114,116,102,98,110};

  // uint8_t th4[100] = {180,194,161,165,207,152,178,136,132,167,205,137,160,146,153,130,141,166,164,126,154,156,155,158,158,
  //                   200,135,186,138,156,157,198,213,141,160,242,155,151,143,150,145,187,155,139,128,167,167,136,239,177,145,141,
  //                   145,168,151,176,160,176,163,176,160,239,141,168,162,180,150,148,132,128,165,167,182,162,182,152,158,140,128,
  //                   150,170,170,162,162,144,128,186,180,185,138,145,135,145,164,130,153,150,138,141,150};


  // use the new image to get medians.
  for (int z = 0; z < _height; z++) {
    int slice_sum = 0;
    int* histogram = static_cast<int*>(calloc(256, sizeof(int)));
    
    // Background subtract
    // background_subtract(input_image_stack[z], _rows, _cols, 100, false, true, true);
    
    for (int xy = 0; xy < _rows*_cols; xy++) {
      uint8_t v = input_image_stack[z][xy];
      histogram[v] += 1;
    }
   
    frame_stds[z] = slice_standard_dev(histogram, 256, _rows * _cols);
    printf("The std for slice %d is %d\n", z, frame_stds[z]);
    
    uint8_t median = 0;
    uint8_t another_thresh = 0;
    uint8_t another_thresh2 = 0;
    // prefix sum.
    for (int i = 1; i < 256; i++) {
      histogram[i] += histogram[i-1];
      if (histogram[i] > (1*_rows*_cols)/2 && median == 0) {
        median = i;
      }

      if (histogram[i] > ((_rows*_cols)/2)*1.05 &&
        another_thresh == 0) {
        another_thresh = i;
      }

      if (histogram[i] > ((_rows*_cols)/2)*0.950 &&
        another_thresh2 == 0) {
        another_thresh2 = i;
      }

    }

    printf("The median for slice %d is %d\n", z, median);
    printf("The another_thresh for slice %d is %d\n", z, another_thresh);
    printf("The another_thresh2 for slice %d is %d\n", z, another_thresh2);
    frame_medians[z] = median;
    //frame_medians2[z] = another_thresh;
    //frame_medians3[z] = another_thresh2;
    //frame_medians2[z] = median+10;
    //frame_medians3[z] = median-10;
    frame_medians2[z] = median+10;
    frame_medians3[z] = median-15;
    free(histogram);

    // frame_threshold_1[z] = th1[z];
    // frame_threshold_2[z] = th2[z];
    // frame_threshold_3[z] = th3[z];
    // frame_threshold_4[z] = th4[z];

  }
  printf("before cilk for\n"); 
  for (int z = 0; z < _height; z++) {
    for (int xy = 0; xy < _rows*_cols; xy++) {
       input_image_stackc1[z][xy] = input_image_stack[z][xy]; 
       input_image_stackc2[z][xy] = input_image_stack[z][xy]; 
       input_image_stackc3[z][xy] = input_image_stack[z][xy]; 
    }
  }
  printf("after cilk for\n"); 
  graph = row_major_to_graph(input_image_stack, rows, cols, height);

  // Compute a graph coloring.
  int colorCount = graph->compute_coloring_grid(_rows, _cols, _height);
  //int colorCount = graph->compute_trivial_coloring();
  printf("The color count is %d\n", colorCount);

  // compute weak average values.
  scheduler =
      new Scheduler(graph->vertexColors, colorCount, graph->num_vertices());
  e = new engine<vdata, edata>(graph, scheduler);
  cilk_for (int i = 0; i < _height*_rows*_cols; i++) {
    scheduler->add_task(i, &compute_weak_value_update);
  }

  e->run();
  printf("done computing weak values\n");

  int used_labels = 1;
  int last_z = 0;
  int last_xy = 0;

  // Call the segmentation function.
  label_objects();

  // lets count the distinct labels.
  uint16_t* label_array =
      static_cast<uint16_t*>(calloc(_height*_rows*_cols, sizeof(uint16_t)));
  uint8_t* label_array_c1 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  uint8_t* label_array_c2 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  uint8_t* label_array_c3 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  int* label_sizes =
      static_cast<int*>(calloc(_height*_rows*_cols, sizeof(int)));

  printf("Assigning a color for each distinct label.\n");

  // eliminate labels that are too small.
  cilk_for (int z = 0; z < _height; z++) {
    cilk_for (int i = 0; i < _rows*_cols; i++) {
      int pixel = z*_rows*_cols + i;
      if (graph->getVertexData(pixel)->object_label > 0) {
        int label = graph->getVertexData(pixel)->object_label;
        __sync_fetch_and_add(&label_sizes[label], 1);
      }
    }
  }

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
        graph->getVertexData(v)->object_label > 0) {
      if (label_sizes[label] >= LABEL_MIN_PIXEL_COUNT) {
        label_array_c1[label] = (uint8_t)rand();
        label_array_c2[label] = (uint8_t)rand();
        label_array_c3[label] = (uint8_t)rand();
        distinct_labels++;
        label_array[label] = distinct_labels;
      }
    }
  }
  printf("Number of distinct labels %d\n", distinct_labels);
  printf("Coloring the graph \n");

  cilk_for (int z = 0; z < _height; z++) {
    cilk_for (int i = 0; i < _rows*_cols; i++) {
      int pixel = z*_rows*_cols + i;
      if (graph->getVertexData(pixel)->object_label > 0) {
        int label = graph->getVertexData(pixel)->object_label;
        input_image_stackc1[z][i] = label_array_c1[label];
        input_image_stackc2[z][i] = label_array_c2[label];
        input_image_stackc3[z][i] = label_array_c3[label];
        if (label_sizes[label] >= LABEL_MIN_PIXEL_COUNT) {
          input_image_stack_labels[z][i] = label_array[label];
        }
      }
    }
  }

  cilk_for (int i = 0; i < height; i++) {
    cv::Mat tmpc1 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc1[i]);
    cv::Mat tmpc2 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc2[i]);
    cv::Mat tmpc3 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc3[i]);
    std::string path = OUTPUT_IMAGE_DIR + "/" + OUTPUT_IMAGE_FILENAME_PREFIX +
        NumberToString(used_labels) + "-" + NumberToString(i) + ".jpg";
    std::string pathtif = OUTPUT_IMAGE_DIR + "/" +
        OUTPUT_IMAGE_FILENAME_PREFIX + NumberToString(used_labels) + "-" +
        NumberToString(i) + ".tif";
    cv::Mat tmp = colorize(tmpc1, tmpc2, tmpc3);
    printf("imwriting to %s\n", path.c_str());
    imwrite(path.c_str(), tmp);
    cv::Mat tmp2 = cv::Mat(_rows, _cols, CV_16U, input_image_stack_labels[i]);
    imwrite(pathtif.c_str(), tmp2);
  }
  return 0;
}

