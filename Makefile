CC=g++
CFLAGS= -O3 
CILK=g++
CILKFLAGS= -w -O3 -g -fcilkplus -lcilkrts -ltcmalloc
LDFLAGS= -lopencv -lopencv_core -lopencv_highgui

#LDFLAGS=`pkg-config opencv --libs --cflags`

DEPS= engine.h Graph.h
OBJ= engine.o Graph.o

all: main

%.o: %.cpp $(DEPS)
	$(CILK) $(CILKFLAGS) -c -o $@ $<

main : $(OBJ) main.cpp Graph.cpp Graph.h graph_properties.h segmentation.cpp scheduler.cpp scheduler.h engine.cpp engine.h background_subtracter.h  Makefile
	$(CILK) $(CILKFLAGS) `pkg-config --cflags opencv` $@.cpp `pkg-config --libs opencv` -o $@ $< 

clean :
	rm -f main *~ *.o

lint :
	python cpplint.py main.cpp segmentation.cpp engine.h Graph.h graph_properties.h imageGraphIO.h

test :
	./run_test.sh
