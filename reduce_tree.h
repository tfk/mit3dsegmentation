// Copyright (c) 2013, Tim Kaler - MIT License

#include <cilk/reducer.h>

#ifndef REDUCE_TREE_H
#define REDUCE_TREE_H


template <typename T>
struct Reduce_Tree_Node{
  Reduce_Tree_Node<T>* left;
  Reduce_Tree_Node<T>* right;
  SPA_reference x;
  int subtree_size;
  bool init;
};

template <typename T>
struct _Reduce_Tree_Node{
  Reduce_Tree_Node<T>* ref;
};

template <typename T>
class Reduce_Tree
{
   struct Monoid: cilk::monoid_base<_Reduce_Tree_Node<T> >
   {
     static void reduce (_Reduce_Tree_Node<T> *left, _Reduce_Tree_Node<T> *right) {
       if (right->ref == NULL) return;
       if (left->ref == NULL) {
         left->ref = right->ref;
         return;
       }

       Reduce_Tree_Node<T>* tmp = left->ref;

       //Reduce_Tree_Node<T>* new_node = new Reduce_Tree_Node<T>[1];
       Reduce_Tree_Node<T>* new_node = (Reduce_Tree_Node<T>*) malloc(sizeof(Reduce_Tree_Node<T>)*1);
       new_node->left = left->ref;
       new_node->right = right->ref;
       new_node->init = false;
       new_node->x.active = false;
       new_node->subtree_size = left->ref->subtree_size + right->ref->subtree_size + 1;

       left->ref = new_node;
     }

     static void identity (_Reduce_Tree_Node<T> *p) {
       Reduce_Tree_Node<T>* new_node = (Reduce_Tree_Node<T>*) malloc(sizeof(Reduce_Tree_Node<T>)*1);
       p->ref = new_node;
       p->ref->left = NULL;
       p->ref->right = NULL;
       p->ref->subtree_size = 1;
       p->ref->init = false;
       p->ref->x.active = false;
     }


   };

private:
   cilk::reducer<Monoid> imp_;

public:
   Reduce_Tree<T>() : imp_() {}

   T* get_spa_reference() {
     return &(imp_.view().ref->x);
   }

  bool do_init() {
     if (imp_.view().ref == NULL) {
       Reduce_Tree_Node<T>* new_node = (Reduce_Tree_Node<T>*) malloc(sizeof(Reduce_Tree_Node<T>)*1);
       imp_.view().ref = new_node;
       imp_.view().ref->left = NULL;
       imp_.view().ref->right = NULL;
       imp_.view().ref->subtree_size = 1;
       imp_.view().ref->init = false;
       imp_.view().ref->x.active = false;
     }
     if (imp_.view().ref->init) return true;
     imp_.view().ref->init = true;
     return false;
   }

   Reduce_Tree_Node<T>* get_root() {
     do_init();
     return imp_.view().ref;
   }

   int traverse(Reduce_Tree_Node<T>* node, T* array, int index) {
     //assert(index >= 0);
     array[index] = node->x;
     array[index].init = true;
     int depth = 1;
     int ldepth = 0;
     int rdepth = 0;
     if (node->left != NULL) {
       int right_sum = node->subtree_size - node->left->subtree_size;
       int left_index = index - right_sum;
       if (node->right != NULL && node->right->x.active && node->left->x.active) {
         ldepth = cilk_spawn traverse(node->left, array, left_index);
         //depth = 1;
       } else {
         ldepth = traverse(node->left, array, left_index);
       }
     }
     if (node->right != NULL) {
       rdepth = traverse(node->right, array, index-1);
     }
     cilk_sync;
     free(node);
     //free(node);
     if (ldepth > rdepth) {
       return depth + ldepth;
     } else {
       return depth + rdepth;
     }
   }

   void clear() {
     imp_.view().ref = NULL;
   }

   T* flatten() {
     Reduce_Tree_Node<T>* root = get_root();
     //assert (root->subtree_size > 0);
     T* flattened_array = (T*) malloc(sizeof(T)*root->subtree_size);
     //T* flattened_array = new T[root->subtree_size];
     // For debugging.
     /*     for (int i = 0; i < root->subtree_size; i++) {
       flattened_array[i].init = false;
     }*/
     //printf("flattened array will have size %d\n", root->subtree_size);

     int d = traverse(root, flattened_array, root->subtree_size - 1);
     printf("the depth of the reduce tree is %d\n", d);
     // For debugging.
     /*for (int i = 0; i < root->subtree_size; i++) {
       assert(flattened_array[i].init);
     }*/
     return flattened_array;
   }

/*   void set(int x, int y) {
     point &p = imp_.view();
     p.set(x, y);
   }*/
//   bool is_valid() { return imp_.view().is_valid(); }
//   int x() { return imp_.view().x(); }
//   int y() { return imp_.view().y(); }
}; // class point_holder


#endif
