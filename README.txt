*How to run

Make sure that you have the input and output folders setup on your local
machine. If you're on cloud6 or cloud12 this has already been done.

Edit run.sh to make modify the paths to reflect those set in main. You
may need to add a folder to your public_html to properly copy over
the files to be viewed in your web browser. If you're not on cloud6
or cloud12 you'll need to edit the path to the ground truth labels file.

run 'make'

run './run.sh'

The shell script run.sh runs the segmentation algorithm and computes the
adapted rand error of the segmentation compared to ground truth.

*Code Structure

The code in main.cpp reads the image into a graph representation and
then calls a function 'label_objects' that resides in segmentation.cpp.

The graph provided to the label_objects function associates data with each
vertex. The pixel value of the vertex (i.e. its pixel value in the original
grayscale image) is provided in the field pixel_value. The label_objects
function assigns pixels to objects by changing their 'object_label' field.

After label_objects sets the vertices object_label fields, the image is
post-processed again by main to remove very small objects and to write
the output images.

After main returns, the run.sh shell script runs a matlab program to compute
the adapted rand error of the produced segmentation in comparison to ground
truth.

*How to edit.

To try a new segmentation technique, just write a new function in
segmentation.cpp and then modify label_objects to call your function.
