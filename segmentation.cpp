// Copyright 2014

#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cilk/cilk.h>

#include <vector>
#include <map>
#include <set>
#include <queue>

#include "./multibag.h"
#include "./graph_properties.h"
#include "./Graph.h"
#include "./engine.h"

#define THRESHOLD_TO_STD_RATIO 1

// Data-graph computation update functions.
void pagerank_update(int vid,
                     void* scheduler_void);
void pagerank_update2(int vid,
                     void* scheduler_void);
void floodfill_update(int vid,
                     void* scheduler_void);
void compute_weak_value_update(int vid,
                     void* scheduler_void);
void merge_regions_update(int vid,
                     void* scheduler_void);

// Parameters for the pagerank phase.
double termination_bound = 1e-9;
//double random_reset_prob = 0.05;   // PageRank random reset probability
double random_reset_prob = 0.15;   // PageRank random reset probability
static int pagerank_mode = 0;

Multibag<int>* border_bag;

/*
void detect_membrane_update(int vid, void* scheduler_void) {
  int w_height = 10;
  int w_width = 10;

  vdata* vd = graph->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;

  int rows = _rows;
  int cols = _cols;
  int height = _height;

  std::vector<int> window;
  std::set<int> window_set;

  float sum = 0;
  int count = 0;
  for (int dx = -w_width; dx <= w_width; dx++) {
    for (int dy = -w_height; dy <= w_height; dy++) {
      if (y+dy < 0 || y+dy >= rows) continue;
      if (x+dx < 0 || x+dx >= cols) continue;
      int dz = 0;
      int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
      uint8_t pixel_value = graph->getVertexData(pixel)->pixel_value;
      sum += pixel_value;
      count += 1;
    }
  }
  float average = sum/count;
  float variance = 0;
  for (int dx = -w_width; dx <= w_width; dx++) {
    for (int dy = -w_height; dy <= w_height; dy++) {
      if (y+dy < 0 || y+dy >= rows) continue;
      if (x+dx < 0 || x+dx >= cols) continue;
      int dz = 0;
      int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
      uint8_t pixel_value = graph->getVertexData(pixel)->pixel_value;
      variance += pow(pixel_value - average, 2);
    }
  }
  variance = variance/count;
  float std = sqrt(variance);
  //printf("the std %f\n", std);
  graph->getVertexData(vid)->window_std = std;
  graph->getVertexData(vid)->window_avg = average;
}
*/

uint8_t* average_values;
int* label_countX;
int* label_countY;
int* label_countZ;

void merge_regions_update_masscenter(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
  vdata* vd = graph->getVertexData(vid);
  int z = vd->z;
  int x = vd->x;
  int y = vd->y;
  // Get the neighboring labels.
  std::vector<int> neighbors;
  graph->getGridEdges(vid, &neighbors);

  int my_label = vd->object_label;

  for (int i = 0; i < neighbors.size(); i++) {
    int n_label = graph->getVertexData(neighbors[i])->object_label;
    //if (graph->getVertexData(neighbors[i])->z != z && (graph->getVertexData(neighbors[i])->x != x || graph->getVertexData(neighbors[i])->y != y)) continue;
    if (graph->getVertexData(neighbors[i])->z != z) continue;
    if (n_label > 0 && n_label != my_label/* && abs(average_values[n_label]-average_values[my_label]) < 5*/) {
    //if ((graph->getVertexData(neighbors[i])->weak_min_value > average_values[n_label] &&
    //    graph->getVertexData(vid)->weak_min_value > average_values[my_label])) {
    if (pow(pow(label_countX[my_label]-label_countX[n_label],2) +
        pow(label_countY[my_label]-label_countY[n_label], 2) +
        pow(label_countZ[my_label]-label_countZ[n_label],2), 0.5) < 200) {
      border_bag->insert(my_label, n_label);
    }
    }
  }
}

void merge_regions_update(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
  vdata* vd = graph->getVertexData(vid);
  int z = vd->z;
  int x = vd->x;
  int y = vd->y;
  // Get the neighboring labels.
  std::vector<int> neighbors;
  graph->getGridEdges(vid, &neighbors);

  int my_label = vd->object_label;

  for (int i = 0; i < neighbors.size(); i++) {
    int n_label = graph->getVertexData(neighbors[i])->object_label;
    //if (graph->getVertexData(neighbors[i])->z != z && (graph->getVertexData(neighbors[i])->x != x || graph->getVertexData(neighbors[i])->y != y)) continue;
    if (graph->getVertexData(neighbors[i])->z != z) continue;
    if (n_label > 0 && n_label != my_label/* && abs(average_values[n_label]-average_values[my_label]) < 5*/) {
    if ((graph->getVertexData(neighbors[i])->weak_min_value > average_values[n_label] &&
        graph->getVertexData(vid)->weak_min_value > average_values[my_label])) {
      border_bag->insert(my_label, n_label);
    }
    }
  }
}

// An update function for pre-processing the vertices to compute
//   average and minimum pixel values in its neighborhood.
void compute_weak_value_update(int vid,
                     void* scheduler_void) {
  //graph->getVertexData(vid)->deleted = false;
  int x = graph->getVertexData(vid)->x;
  int y = graph->getVertexData(vid)->y;
  int z = graph->getVertexData(vid)->z;

  int sum = 0;
  uint8_t min = 255;
  int count = 0;
  for (int dx = -4; dx <= 4; dx++) {
    for (int dy = -4; dy <= 4; dy++) {
      if (dx*dx+dy*dy > 18) continue;
      //if (dx*dx+dy*dy > 25) continue;
      if (y+dy < 0 || y+dy >= _rows) continue;
      if (x+dx < 0 || x+dx >= _cols) continue;
      //if (z+dz < 0 || z+dz >= _height) continue;
      count++;
      uint8_t v = graph->getVertexData(vid + dy*_cols + dx)->pixel_value;
      sum += v;
      if (dx*dx+dy*dy > 10) continue;
      if (v < min) min = v;
    }
  }
  graph->getVertexData(vid)->weak_min_value = min;
  graph->getVertexData(vid)->weak_average_value = (uint8_t) (sum / count);
  //graph->getVertexData(vid)->weak_average_value = (uint8_t) (sum / 49);


  // compute the strong averages
  sum = graph->getVertexData(vid)->pixel_value;
  min = 255;
  count = 0;
  for (int dx = -5; dx <= 5; dx++) {
    for (int dy = -5; dy <= 5; dy++) {
    //if (dx*dx+dy*dy > 81) continue;
    if (dx*dx+dy*dy > 32) continue;
    if (y+dy < 0 || y+dy >= _rows) continue;
    if (x+dx < 0 || x+dx >= _cols) continue;
    //if (z+dz < 0 || z+dz >= _height) continue;
    count++;
    uint8_t v = graph->getVertexData(vid + dy*_cols + dx)->pixel_value;
    sum += v;
    if (dx*dx+dy*dy > 25) continue;
    if (v < min) min = v;
    }
  }

  graph->getVertexData(vid)->strong_min_value = min;
  //graph->getVertexData(vid)->strong_average_value = (uint8_t) (sum / 81);
  graph->getVertexData(vid)->strong_average_value = (uint8_t) (sum / count);
}

/**
 * The flood fill merge update function - unclear if we should do this at all.
 */
void floodfill_merge_update(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
  vdata* vd = graph->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;

  std::vector<int> neighbors; std::vector<int> other_neighbors;

  graph->getGridEdges(vid, &other_neighbors);
  graph->getGridEdges(vid, &neighbors);

  int in_degree = neighbors.size();
  int effective_degree = neighbors.size();
  int out_degree = neighbors.size();
  int my_label = graph->getVertexData(vid)->object_label;

  int min_neighbor_label = my_label;

  // get the minimum neighbor label.
  for (int i = 0; i < out_degree; i++) {
    vdata* neighbor_vdata = graph->getVertexData(neighbors[i]);
    if (neighbor_vdata->object_label != 0 &&
        neighbor_vdata->object_label < min_neighbor_label) {
      min_neighbor_label = neighbor_vdata->object_label;
    }
  }

  // check if we need to schedule neighboring pixels for another update.
  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    int n_label = neighbor_vdata->object_label;
    if (n_label > 0 && n_label > min_neighbor_label) {
      scheduler->add_task(other_neighbors[i], &floodfill_update);
      if (terminated) {
        terminated = false;
      }
    }
  }
  // set my new label to be the min neighbor label.
  graph->getVertexData(vid)->object_label = min_neighbor_label;
}  // end of pagerank update function

/**
 * The flood fill update function
 */
void floodfill_update(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
  vdata* vd = graph->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;

  std::vector<int> neighbors;

  std::vector<int> other_neighbors;
  graph->getGridEdges(vid, &other_neighbors);

  // Filter neighbors.
  for (int i = 0; i < other_neighbors.size(); i++) {
    int pixel = other_neighbors[i];
    int new_z = graph->getVertexData(pixel)->z;
    float weight = 0;
    bool same_z = new_z == z;
    //int thresh = same_z ? frame_medians[new_z]+15 : frame_medians[new_z]+15;

    int thresh = same_z ? frame_medians2[new_z] : frame_medians2[new_z];
    int thresh_min = same_z ? frame_medians3[new_z] : frame_medians[new_z];

/*    int thresh = graph->getVertexData(pixel)->window_avg + 15;
    int thresh_min = same_z ? frame_medians3[new_z] : frame_medians[new_z];*/
    // int thresh = same_z ? frame_medians[new_z]+15 : frame_medians[new_z]+15;
    // int thresh = same_z ? frame_medians[new_z]-THRESHOLD_TO_STD_RATIO*frame_stds[new_z] : frame_medians[new_z]-THRESHOLD_TO_STD_RATIO*frame_stds[new_z];
    // int thresh = frame_threshold_4[new_z];

    // int thresh_min = same_z ? frame_medians[new_z]-5 : frame_medians[new_z];
    // int thresh_min = frame_threshold_2[new_z]; 

    uint8_t pixel_average = same_z ?
        graph->getVertexData(pixel)->weak_average_value :
        graph->getVertexData(pixel)->strong_average_value;
    //uint8_t pixel_average = graph->getVertexData(pixel)->pixel_value;
    uint8_t pixel_min = same_z ?
        graph->getVertexData(pixel)->weak_min_value :
        graph->getVertexData(pixel)->strong_min_value;

    if (/*graph->getVertexData(pixel)->deleted || */pixel_average < thresh ||
        pixel_min < thresh_min) {
      weight = 0;
    } else {
      weight = 1;
    }

/*    if (graph->getVertexData(pixel)->window_std >= 35) {
      weight = 0;
    } else if (graph->getVertexData(pixel)->window_std < 25) {
      weight = 1;
    }
*/
    if (weight > 0) {
      neighbors.push_back(pixel);
    }
  }

  int in_degree = neighbors.size();
  int effective_degree = neighbors.size();
  int out_degree = neighbors.size();
  int my_label = graph->getVertexData(vid)->object_label;

  int min_neighbor_label = my_label;

  // get the minimum neighbor label.
  for (int i = 0; i < out_degree; i++) {
    vdata* neighbor_vdata = graph->getVertexData(neighbors[i]);
    if (neighbor_vdata->object_label != 0 &&
        neighbor_vdata->object_label < min_neighbor_label) {
      min_neighbor_label = neighbor_vdata->object_label;
    }
  }


  // check if we need to schedule neighboring pixels for another update.
  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    if (neighbor_vdata->object_label > min_neighbor_label) {
      scheduler->add_task(other_neighbors[i], &floodfill_update);
      if (terminated) {
        terminated = false;
      }
    }
  }
  // set my new label to be the min neighbor label.
  graph->getVertexData(vid)->object_label = min_neighbor_label;
}  // end of pagerank update function

// determine the distance between u and v.
int pagerank_pixel_distance (int u, int v) {
  if (graph->getVertexData(u)->z == graph->getVertexData(v)->z) {
    return abs(graph->getVertexData(u)->strong_average_value -
        graph->getVertexData(v)->strong_average_value) + 1;
  } else {
    return abs(graph->getVertexData(u)->weak_average_value -
        graph->getVertexData(v)->weak_average_value) + 2;
  }
}

// determine the distance between u and v.
float pagerank_pixel_distance_inv (int u, int v) {
  return ((float) 1) / pow(pagerank_pixel_distance(u,v),2);
}

/**
 * The Page rank update function 2nd design.
 */
void pagerank_update2(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);

  // Get the data associated with the vertex
  vdata* vd = graph->getVertexData(vid);
  vd->p = vd->p + vd->r * random_reset_prob;

  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int num_z_neighbors = 0;
  std::vector<int> neighbors;
  std::vector<int> other_neighbors;
  graph->getGridEdges(vid, &other_neighbors);

  // Filter
  float total_degree = 0;
  for (int i = 0; i < other_neighbors.size(); i++) {
    int pixel = other_neighbors[i];
    vdata* nvd = graph->getVertexData(pixel);
    float weight = 0;
    if (pagerank_pixel_distance(vid, pixel) > 11 ||
        graph->getVertexData(pixel)->object_label != vd->object_label) {
      weight = 0;
    } else {
      weight = 1;
    }
    if (weight > 0) {
      if (nvd->z != vd->z
          && graph->getVertexData(pixel)->object_label == vd->object_label) {
        num_z_neighbors++;
      }
      neighbors.push_back(pixel);
      total_degree += pagerank_pixel_distance_inv(vid, pixel);
    }
  }


  int in_degree = neighbors.size();
  //float effective_degree = total_degree;
  float effective_degree = neighbors.size();
  int out_degree = neighbors.size();

  // Policy: Z directions get just 1/2 of probability mass.
  // NOTE(TFK): Unclear if this is an important heuristic. Perhaps try
  //            eliminating it.
  for (int i = 0; i < out_degree; i++) {
    vdata* neighbor_vdata = graph->getVertexData(neighbors[i]);
    // Compute the contribution for the neighbor, and add it to the sum.
    // only push potential to neighbors of the same label.
    if (neighbor_vdata->object_label != vd->object_label) continue;

    float distance = pagerank_pixel_distance_inv(vid, neighbors[i]);
    distance = 1;
    neighbor_vdata->r = neighbor_vdata->r +
        (distance*(1-random_reset_prob)/(2*effective_degree)) * vd->r;
/*
    if (neighbor_vdata->z != vd->z) {
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*num_z_neighbors)) * 0.5 * vd->r;
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*num_z_neighbors)) * 0.5 * vd->r;
    } else {
      // Remember this value as last read from the neighbor.
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*(out_degree-num_z_neighbors))) * .5 * vd->r;
    }*/
  }

  vd->r = vd->r*((1-random_reset_prob)/2);
  vd->last_r = vd->r;
  int max_label = vd->object_label;
  double max_label_value = (vd->p + vd->r)*2;
  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    double neighbor_label_value = neighbor_vdata->p+neighbor_vdata->r;
    if (neighbor_label_value > max_label_value &&
        neighbor_vdata->object_label > 0
        && (neighbor_label_value > max_label_value || max_label == 0) ) {
      max_label_value = neighbor_label_value;
      max_label = neighbor_vdata->object_label;
    }
  }

  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    double diff = neighbor_vdata->last_r - neighbor_vdata->r;
    // Schedule an update at a neighbor if its value changed sufficiently,
    //   or if this vertex's label changed.
    //if (diff > termination_bound/8 || max_label != vd->object_label) {
    if (diff > termination_bound || max_label != vd->object_label) {
      scheduler->add_task(other_neighbors[i], &pagerank_update2);
      if (terminated) {
        terminated = false;
      }
    }
  }

  // Set this vertex's new object label.
  vd->object_label = max_label;
}  // end of pagerank update function


/**
 * The Page rank update function
 */
void pagerank_update(int vid,
                     void* scheduler_void) {
  Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);

  // Get the data associated with the vertex
  vdata* vd = graph->getVertexData(vid);
  vd->p = vd->p + vd->r * random_reset_prob;

  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int num_z_neighbors = 0;
  std::vector<int> neighbors;
  std::vector<int> other_neighbors;
  graph->getGridEdges(vid, &other_neighbors);

  // Filter
  for (int i = 0; i < other_neighbors.size(); i++) {
    int pixel = other_neighbors[i];
    vdata* nvd = graph->getVertexData(pixel);
    float weight = 0;
    if (abs(nvd->pixel_value - vd->pixel_value > 10) ||
        graph->getVertexData(pixel)->object_label != vd->object_label) {
      weight = 0;
    } else {
      weight = 1;
    }
    if (weight > 0) {
      if (nvd->z != vd->z
          && graph->getVertexData(pixel)->object_label == vd->object_label) {
        num_z_neighbors++;
      }
      neighbors.push_back(pixel);
    }
  }

  int in_degree = neighbors.size();
  int effective_degree = neighbors.size();
  int out_degree = neighbors.size();

  // Policy: Z directions get just 1/2 of probability mass.
  // NOTE(TFK): Unclear if this is an important heuristic. Perhaps try
  //            eliminating it.
  for (int i = 0; i < out_degree; i++) {
    vdata* neighbor_vdata = graph->getVertexData(neighbors[i]);
    // Compute the contribution for the neighbor, and add it to the sum.
    // only push potential to neighbors of the same label.
    if (neighbor_vdata->object_label != vd->object_label) continue;
    neighbor_vdata->r = neighbor_vdata->r +
        ((1-random_reset_prob)/(2*effective_degree)) * vd->r;
/*
    if (neighbor_vdata->z != vd->z) {
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*num_z_neighbors)) * 0.5 * vd->r;
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*num_z_neighbors)) * 0.5 * vd->r;
    } else {
      // Remember this value as last read from the neighbor.
      //neighbor_vdata->r = neighbor_vdata->r +
      //    ((1-random_reset_prob)/(2*(out_degree-num_z_neighbors))) * .5 * vd->r;
    }*/
  }

  vd->r = vd->r*((1-random_reset_prob)/2);
  vd->last_r = vd->r;
  int max_label = vd->object_label;
  double max_label_value = vd->p + vd->r;
  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    double neighbor_label_value = neighbor_vdata->p+neighbor_vdata->r;
    if (neighbor_label_value > max_label_value &&
        neighbor_vdata->object_label > 0
        && (neighbor_label_value > max_label_value || max_label == 0) ) {
      max_label_value = neighbor_label_value;
      max_label = neighbor_vdata->object_label;
    }
  }

  for (int i = 0; i < other_neighbors.size(); i++) {
    vdata* neighbor_vdata = graph->getVertexData(other_neighbors[i]);
    double diff = neighbor_vdata->last_r - neighbor_vdata->r;
    // Schedule an update at a neighbor if its value changed sufficiently,
    //   or if this vertex's label changed.
    if (diff > termination_bound/8 || max_label != vd->object_label) {
      scheduler->add_task(other_neighbors[i], &pagerank_update);
      if (terminated) {
        terminated = false;
      }
    }
  }

  // Set this vertex's new object label.
  vd->object_label = max_label;
}  // end of pagerank update function


// Merge adjacent floodfill skeletons.
void merge_floodfill_regions() {
  printf("About to merge floodfill regions in image\n");
  cilk_for (int i = 0; i < graph->num_vertices(); i++) {
    int label = graph->getVertexData(i)->object_label;
    if (label > 0) {
      scheduler->add_task(i, &floodfill_merge_update);
    }
  }
  e->run();
  printf("done running merge floodfill regions step\n");
}

// Use floodfill to label a skeleton of the region
void label_region_of_seed_floodfill(std::vector<int> seed_vertices) {
  printf("About to run floodfill on image\n");

  // reset the graph
  cilk_for (int i = 0; i < graph->num_vertices(); i++) {
    graph->getVertexData(i)->p = 0;
    graph->getVertexData(i)->r = 0;
    graph->getVertexData(i)->object_label = 0;
  }


  // Give initial labels to all the seed vertices.
  cilk_for (int i = 0; i < seed_vertices.size(); i++) {
    graph->getVertexData(seed_vertices[i])->object_label = i+1;
    scheduler->add_task(seed_vertices[i], &floodfill_update);
  }

  double start = tfk_get_time();
  e->run();
  double end = tfk_get_time();
  printf("done running\n");
}

void label_region_of_seed2() {
  printf("About to run pagerank on image\n");
  // reset the graph
  cilk_for (int i = 0; i < graph->num_vertices(); i++) {
    graph->getVertexData(i)->p = 0;
    graph->getVertexData(i)->r = 0;
    graph->getVertexData(i)->skeleton = false;
    if (graph->getVertexData(i)->object_label > 0) {
      graph->getVertexData(i)->r = 1;
      graph->getVertexData(i)->skeleton = true;
    }
    scheduler->add_task(i, &pagerank_update2);
  }

  double start = tfk_get_time();
  e->run();
  double end = tfk_get_time();
  printf("done running\n");
}

void label_region_of_seed() {
  printf("About to run pagerank on image\n");
  // reset the graph
  cilk_for (int i = 0; i < graph->num_vertices(); i++) {
    graph->getVertexData(i)->p = 0;
    graph->getVertexData(i)->r = 0;
    graph->getVertexData(i)->skeleton = false;
    if (graph->getVertexData(i)->object_label > 0) {
      graph->getVertexData(i)->r = 1;
      graph->getVertexData(i)->skeleton = true;
    }
    scheduler->add_task(i, &pagerank_update);
  }

  double start = tfk_get_time();
  e->run();
  double end = tfk_get_time();
  printf("done running\n");
}

void merge_small_objects() {
  uint32_t* label_array =
      static_cast<uint32_t*>(calloc(_height*_rows*_cols, sizeof(uint32_t)));

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
        graph->getVertexData(v)->object_label > 0) {
        distinct_labels++;
        label_array[label] = distinct_labels;
    }
    // perform a relabeling to compact space.
    if (label > 0) {
      graph->getVertexData(v)->object_label = label_array[label];
    }
  }
  free(label_array);
  printf("Number of distinct labels %d\n", distinct_labels);
  // get average values for labels
  int* label_counts = (int*) calloc(distinct_labels+1, sizeof(int));
  int* label_counts2 = (int*) calloc(distinct_labels+1, sizeof(int));
  cilk_for(int v = 0; v < _height*_rows*_cols; v++) {
    __sync_fetch_and_add(&(label_counts[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->pixel_value);
    __sync_fetch_and_add(&(label_counts2[graph->getVertexData(v)->object_label]),
        1);
  }
  average_values = (uint8_t*) malloc(sizeof(uint8_t) * (distinct_labels+1));
  for (int i = 0; i < distinct_labels; i++) {
    if (label_counts2[i] > 0)
    average_values[i] = label_counts[i]/label_counts2[i];
  }
  free(label_counts);
  free(label_counts2);

  int num_labels = distinct_labels+100;

  int* label_sizes = (int*) calloc(num_labels, sizeof(int));
  for (int i = 0; i < _height*_rows*_cols; i++) {
    label_sizes[graph->getVertexData(i)->object_label]++;
  }

  // Allocate a multibag with 70k colors (estimate).
  border_bag = new Multibag<int>(num_labels);

  printf("start running merge labeled regions\n");
  for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    scheduler->add_task(xyz, &merge_regions_update);
  }
  e->run();
  free(average_values);
  // just in case, make sure each color has one element.
  for (int i = 0; i < num_labels; i++) {
    border_bag->insert(0, i);
  }

  border_bag->collect();

  uint32_t* label_remap = (uint32_t*) calloc(num_labels,sizeof(uint32_t));

  cilk_for (int label = 0; label < num_labels; label++ ) {
    std::vector<std::vector<int>*> vector_list =
        border_bag->get_vector_list(label);
    int detected_neighbor = 0;
    bool fail = false;
    int error_count = 0;
    int border_size = 0;
    std::map<int, int> border_label_map;

    std::vector<uint32_t> distinct_nlabels;

    for (int i = 0; i < vector_list.size(); i++) {
      std::vector<int>* vector = vector_list[i];
      for (int j = 0; j < vector->size(); j++) {
        int n = (*vector)[j];
        if (n != 0) {
          if (border_label_map[n] == 0) distinct_nlabels.push_back(n);
          border_label_map[n]++;
          /*
            if (detected_neighbor == 0) detected_neighbor = n;
            if (detected_neighbor != n) error_count++;
          */
          border_size++;
        }
      }
    }

    // find the neighbor that borders the most.
    int max_border = 0;
    int max_label = 0;
    for (int i = 0; i < distinct_nlabels.size(); i++) {
      int n = distinct_nlabels[i];
      if (border_label_map[n] > max_border) {
        max_border = border_label_map[n];
        max_label = n;
      }
    }

    //if (max_border < 0.4*border_size/* || border_size < 500 && max_border < 0.2*border_size*/) {
    if (/*border_size < 0.1*border_size ||*/ border_size==0 || label_sizes[max_label]  > 150000 || label_sizes[label] > 150000/* || border_size < 500 && max_border < 0.2*border_size*/) {
      fail = true;
    }

    detected_neighbor = max_label;
    if (!fail && detected_neighbor != 0) {
      printf("should merge label %d into %d\n", label, detected_neighbor);
      label_remap[label] = detected_neighbor;
    } else {
      label_remap[label] = label;
    }
  }

  printf("performing relabing using label_remap array\n");
  cilk_for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    uint32_t label = graph->getVertexData(xyz)->object_label;
    if (label > 0) {
      int other_label = label_remap[label];
      // verify that we're not doing a label swap.
      if (label != label_remap[other_label] ||
          label < label_remap[label]) {
        graph->getVertexData(xyz)->object_label = label_remap[label];
      }
    }
  }

  border_bag->clear();
  delete border_bag;

  printf("done running merge labeled regions\n");

}

void merge_labeled_regions_masscenter() {
  uint32_t* label_array =
      static_cast<uint32_t*>(calloc(_height*_rows*_cols, sizeof(uint32_t)));

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
        graph->getVertexData(v)->object_label > 0) {
        distinct_labels++;
        label_array[label] = distinct_labels;
    }
    // perform a relabeling to compact space.
    if (label > 0) {
      graph->getVertexData(v)->object_label = label_array[label];
    }
  }
  free(label_array);
  printf("Number of distinct labels %d\n", distinct_labels);

  // get average values for labels
  int* label_counts = (int*) calloc(distinct_labels+1, sizeof(int));
  int* label_counts2 = (int*) calloc(distinct_labels+1, sizeof(int));

  label_countX = (int*) calloc(distinct_labels+1, sizeof(int));
  label_countY = (int*) calloc(distinct_labels+1, sizeof(int));
  label_countZ = (int*) calloc(distinct_labels+1, sizeof(int));
  cilk_for(int v = 0; v < _height*_rows*_cols; v++) {
    __sync_fetch_and_add(&(label_countX[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->x);
    __sync_fetch_and_add(&(label_countY[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->y);
    __sync_fetch_and_add(&(label_countZ[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->z);
  }
  average_values = (uint8_t*) malloc(sizeof(uint8_t) * (distinct_labels+1));
  for (int i = 0; i < distinct_labels; i++) {
    if (label_counts2[i] > 0) {
    average_values[i] = label_counts[i]/label_counts2[i];
    label_countX[i] = label_countX[i]/label_counts2[i];
    label_countY[i] = label_countY[i]/label_counts2[i];
    label_countZ[i] = label_countZ[i]/label_counts2[i];
    }
    //average_values[i] = 0;
  }
  free(label_counts);
  free(label_counts2);
  int num_labels = distinct_labels+100;

  // Allocate a multibag with 70k colors (estimate).
  border_bag = new Multibag<int>(num_labels);

  printf("start running merge labeled regions\n");
  for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    scheduler->add_task(xyz, &merge_regions_update_masscenter);
  }
  e->run();
  free(average_values);
  free(label_countX);
  free(label_countY);
  // just in case, make sure each color has one element.
  for (int i = 0; i < num_labels; i++) {
    border_bag->insert(0, i);
  }

  border_bag->collect();

  uint32_t* label_remap = (uint32_t*) calloc(num_labels,sizeof(uint32_t));

  cilk_for (int label = 0; label < num_labels; label++ ) {
    std::vector<std::vector<int>*> vector_list =
        border_bag->get_vector_list(label);
    int detected_neighbor = 0;
    bool fail = false;
    int error_count = 0;
    int border_size = 0;
    std::map<int, int> border_label_map;

    std::vector<uint32_t> distinct_nlabels;

    for (int i = 0; i < vector_list.size(); i++) {
      std::vector<int>* vector = vector_list[i];
      for (int j = 0; j < vector->size(); j++) {
        int n = (*vector)[j];
        if (n != 0) {
          if (border_label_map[n] == 0) distinct_nlabels.push_back(n);
          border_label_map[n]++;
          /*
            if (detected_neighbor == 0) detected_neighbor = n;
            if (detected_neighbor != n) error_count++;
          */
          border_size++;
        }
      }
    }

    // find the neighbor that borders the most.
    int max_border = 0;
    int max_label = 0;
    for (int i = 0; i < distinct_nlabels.size(); i++) {
      int n = distinct_nlabels[i];
      if (border_label_map[n] > max_border) {
        max_border = border_label_map[n];
        max_label = n;
      }
    }

    if (max_border < 0.5*border_size || border_size < 100/* || border_size < 500 && max_border < 0.2*border_size*/) {
      fail = true;
    }

    detected_neighbor = max_label;
    if (!fail && detected_neighbor != 0) {
      printf("should merge label %d into %d\n", label, detected_neighbor);
      label_remap[label] = detected_neighbor;
    } else {
      label_remap[label] = label;
    }
  }

  printf("performing relabing using label_remap array\n");
  cilk_for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    uint32_t label = graph->getVertexData(xyz)->object_label;
    if (label > 0) {
      int other_label = label_remap[label];
      // verify that we're not doing a label swap.
      if (label != label_remap[other_label] ||
          label < label_remap[label]) {
        graph->getVertexData(xyz)->object_label = label_remap[label];
      }
    }
  }
  printf("done running merge labeled regions\n");
}

void merge_labeled_regions_conservative() {
  uint32_t* label_array =
      static_cast<uint32_t*>(calloc(_height*_rows*_cols, sizeof(uint32_t)));

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
        graph->getVertexData(v)->object_label > 0) {
        distinct_labels++;
        label_array[label] = distinct_labels;
    }
    // perform a relabeling to compact space.
    if (label > 0) {
      graph->getVertexData(v)->object_label = label_array[label];
    }
  }
  free(label_array);
  printf("Number of distinct labels %d\n", distinct_labels);

  // get average values for labels
  int* label_counts = (int*) calloc(distinct_labels+1, sizeof(int));
  int* label_counts2 = (int*) calloc(distinct_labels+1, sizeof(int));
  cilk_for(int v = 0; v < _height*_rows*_cols; v++) {
    __sync_fetch_and_add(&(label_counts[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->pixel_value);
    __sync_fetch_and_add(&(label_counts2[graph->getVertexData(v)->object_label]),
        1);
  }
  average_values = (uint8_t*) malloc(sizeof(uint8_t) * (distinct_labels+1));
  for (int i = 0; i < distinct_labels; i++) {
    if (label_counts2[i] > 0)
    average_values[i] = label_counts[i]/label_counts2[i];
    //average_values[i] = 0;
  }
  free(label_counts);
  free(label_counts2);
  int num_labels = distinct_labels+100;

  // Allocate a multibag with 70k colors (estimate).
  border_bag = new Multibag<int>(num_labels);

  printf("start running merge labeled regions\n");
  for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    scheduler->add_task(xyz, &merge_regions_update);
  }
  e->run();
  free(average_values);
  // just in case, make sure each color has one element.
  for (int i = 0; i < num_labels; i++) {
    border_bag->insert(0, i);
  }

  border_bag->collect();

  uint32_t* label_remap = (uint32_t*) calloc(num_labels,sizeof(uint32_t));

  cilk_for (int label = 0; label < num_labels; label++ ) {
    std::vector<std::vector<int>*> vector_list =
        border_bag->get_vector_list(label);
    int detected_neighbor = 0;
    bool fail = false;
    int error_count = 0;
    int border_size = 0;
    std::map<int, int> border_label_map;

    std::vector<uint32_t> distinct_nlabels;

    for (int i = 0; i < vector_list.size(); i++) {
      std::vector<int>* vector = vector_list[i];
      for (int j = 0; j < vector->size(); j++) {
        int n = (*vector)[j];
        if (n != 0) {
          if (border_label_map[n] == 0) distinct_nlabels.push_back(n);
          border_label_map[n]++;
          /*
            if (detected_neighbor == 0) detected_neighbor = n;
            if (detected_neighbor != n) error_count++;
          */
          border_size++;
        }
      }
    }

    // find the neighbor that borders the most.
    int max_border = 0;
    int max_label = 0;
    for (int i = 0; i < distinct_nlabels.size(); i++) {
      int n = distinct_nlabels[i];
      if (border_label_map[n] > max_border) {
        max_border = border_label_map[n];
        max_label = n;
      }
    }

    if (max_border < 0.5*border_size || border_size < 100/* || border_size < 500 && max_border < 0.2*border_size*/) {
      fail = true;
    }

    detected_neighbor = max_label;
    if (!fail && detected_neighbor != 0) {
      printf("should merge label %d into %d\n", label, detected_neighbor);
      label_remap[label] = detected_neighbor;
    } else {
      label_remap[label] = label;
    }
  }

  printf("performing relabing using label_remap array\n");
  cilk_for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    uint32_t label = graph->getVertexData(xyz)->object_label;
    if (label > 0) {
      int other_label = label_remap[label];
      // verify that we're not doing a label swap.
      if (label != label_remap[other_label] ||
          label < label_remap[label]) {
        graph->getVertexData(xyz)->object_label = label_remap[label];
      }
    }
  }
  border_bag->clear();
  delete border_bag;
  printf("done running merge labeled regions\n");
}

void merge_labeled_regions() {
  uint32_t* label_array =
      static_cast<uint32_t*>(calloc(_height*_rows*_cols, sizeof(uint32_t)));

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
        graph->getVertexData(v)->object_label > 0) {
        distinct_labels++;
        label_array[label] = distinct_labels;
    }
    // perform a relabeling to compact space.
    if (label > 0) {
      graph->getVertexData(v)->object_label = label_array[label];
    }
  }
  free(label_array);
  printf("Number of distinct labels %d\n", distinct_labels);

  // get average values for labels
  int* label_counts = (int*) calloc(distinct_labels+1, sizeof(int));
  int* label_counts2 = (int*) calloc(distinct_labels+1, sizeof(int));
  cilk_for(int v = 0; v < _height*_rows*_cols; v++) {
    __sync_fetch_and_add(&(label_counts[graph->getVertexData(v)->object_label]),
        graph->getVertexData(v)->pixel_value);
    __sync_fetch_and_add(&(label_counts2[graph->getVertexData(v)->object_label]),
        1);
  }
  average_values = (uint8_t*) malloc(sizeof(uint8_t) * (distinct_labels+1));
  for (int i = 0; i < distinct_labels; i++) {
    //if (label_counts2[i] > 0)
    //average_values[i] = label_counts[i]/label_counts2[i];
    average_values[i] = 0;
  }
  free(label_counts);
  free(label_counts2);
  int num_labels = distinct_labels+100;

  // Allocate a multibag with 70k colors (estimate).
  border_bag = new Multibag<int>(num_labels);

  printf("start running merge labeled regions\n");
  for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    scheduler->add_task(xyz, &merge_regions_update);
  }
  e->run();
  free(average_values);
  // just in case, make sure each color has one element.
  for (int i = 0; i < num_labels; i++) {
    border_bag->insert(0, i);
  }

  border_bag->collect();

  uint32_t* label_remap = (uint32_t*) calloc(num_labels,sizeof(uint32_t));

  cilk_for (int label = 0; label < num_labels; label++ ) {
    std::vector<std::vector<int>*> vector_list =
        border_bag->get_vector_list(label);
    int detected_neighbor = 0;
    bool fail = false;
    int error_count = 0;
    int border_size = 0;
    std::map<int, int> border_label_map;

    std::vector<uint32_t> distinct_nlabels;

    for (int i = 0; i < vector_list.size(); i++) {
      std::vector<int>* vector = vector_list[i];
      for (int j = 0; j < vector->size(); j++) {
        int n = (*vector)[j];
        if (n != 0) {
          if (border_label_map[n] == 0) distinct_nlabels.push_back(n);
          border_label_map[n]++;
          /*
            if (detected_neighbor == 0) detected_neighbor = n;
            if (detected_neighbor != n) error_count++;
          */
          border_size++;
        }
      }
    }

    // find the neighbor that borders the most.
    int max_border = 0;
    int max_label = 0;
    for (int i = 0; i < distinct_nlabels.size(); i++) {
      int n = distinct_nlabels[i];
      if (border_label_map[n] > max_border) {
        max_border = border_label_map[n];
        max_label = n;
      }
    }

    if (max_border < 0.4*border_size/* || border_size < 500 && max_border < 0.2*border_size*/) {
      fail = true;
    }

    detected_neighbor = max_label;
    if (!fail && detected_neighbor != 0) {
      printf("should merge label %d into %d\n", label, detected_neighbor);
      label_remap[label] = detected_neighbor;
    } else {
      label_remap[label] = label;
    }
  }

  printf("performing relabing using label_remap array\n");
  cilk_for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    uint32_t label = graph->getVertexData(xyz)->object_label;
    if (label > 0) {
      int other_label = label_remap[label];
      // verify that we're not doing a label swap.
      if (label != label_remap[other_label] ||
          label < label_remap[label]) {
        graph->getVertexData(xyz)->object_label = label_remap[label];
      }
    }
  }
  border_bag->clear();
  delete border_bag;
  printf("done running merge labeled regions\n");
}

void init_distances() {
  for (int xyz = 0; xyz < _rows*_cols*_height; xyz++) {
    //scheduler->add_task(xyz, &detect_membrane_update, 1);
  }
  e->run();
}

// Modify the object_labels of a vertex.
void segmentation_floodfill() {
  int candidate_count = 0;
  Vector_reducer<int> candidates;
  //init_distances();
  // get all candidates
  // TODO(TFK): Must set weak_average_value to 0 once the pixel is labeled.
  for (int z = 0; z < _height; z++) {
    for (int xy = 0; xy < _rows*_cols; xy++) {
      int pixel = z*_rows*_cols + xy;
      //if (graph->getVertexData(pixel)->deleted) continue;
       // if (graph->getVertexData(pixel)->weak_average_value >= frame_threshold_4[z] &&
       //     graph->getVertexData(pixel)->weak_min_value >= frame_threshold_2[z]) {
       //   candidates.insert(z*_rows*_cols + xy);
      if (graph->getVertexData(pixel)->weak_average_value >=
          frame_medians2[z] &&
          graph->getVertexData(pixel)->weak_min_value >= frame_medians3[z]/* &&
          graph->getVertexData(pixel)->window_std < 35*/) {
        candidates.insert(z*_rows*_cols + xy);
      } /*else {
        printf("the std is %f\n", graph->getVertexData(pixel)->window_std);
      }*/
    }
  }
  candidate_count = candidates.get_reference().size();
  printf("candidate count is %d\n", candidate_count);

  label_region_of_seed_floodfill(candidates.get_reference());
  //merge_floodfill_regions();
  label_region_of_seed2(); // use alternative pagerank

  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  /*merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
*/
  //merge_labeled_regions();
  /*merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();
  merge_labeled_regions();*/
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_small_objects();
  merge_labeled_regions_conservative();
  merge_labeled_regions_conservative();
  merge_labeled_regions_conservative();
  //merge_labeled_regions_masscenter();
  //merge_labeled_regions_masscenter();

}

void label_objects() {
  // Invoke current segmentation method.
  segmentation_floodfill();
}
