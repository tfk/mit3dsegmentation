// Copyright 2014

#ifndef GRAPH_EXTRACTION_GRAPH_PROPERTIES_H_
#define GRAPH_EXTRACTION_GRAPH_PROPERTIES_H_

#include "../Graph.h"
#include "../scheduler.h"
#include "../engine.h"

// Programmer defined vertex and edge structures.
//   x,y,z required.
struct vdata{
  explicit vdata(uint16_t object_label = 0) : object_label(object_label) { }
  int checked;
  int roundNum;
  int scheduledFor;
  int parent;
  int BFSd;
  int BFSparent;
  int level;
  int monotonicD;
  uint16_t object_label;
  uint16_t x;
  uint16_t y;
  uint16_t z;
  uint8_t pixel_value;
  bool deleted;
  bool localmax;
  bool realLocalmax;
};

// This structure isn't really used since we're
// representing the edges implicitly.
struct edata {
  double weight;
  explicit edata(double weight = 1) : weight(weight) { }
};

// The scheduler and graph objects. Global for
// convenience.
static Scheduler* scheduler; Graph<vdata, edata>* graph;
static engine<vdata, edata>* e;
static bool terminated = false;

static int _height = 1;  // Number of images in the stack.
static int _rows = 1024;  // pixel rows in image in stack.
static int _cols = 1024;  // pixel cols in image in stack.

#endif  // GRAPH_EXTRACTION_GRAPH_PROPERTIES_H_
