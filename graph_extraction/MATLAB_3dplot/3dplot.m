xdim=1024;
ydim=1024;
zdim=25;

fileID = fopen('/scratch/graph_extraction_output/output-stack-label.yml');

data=nan(xdim,ydim,zdim);

for z=1:zdim
    for y=1:ydim
        tmp =textscan(fileID,'%d',xdim);
        for x=1:xdim
            data(x,y,z) =  tmp{1}(x);
        end
    end
    disp(z)
end

%%
label=10;

% figure;
% data2=nan(xdim,ydim,zdim);

ix=[];
iy=[];
iz=[];
for z=1:zdim
    for y=1:ydim
        for x=1:xdim
            if data(x,y,z)==label && data2(x,y,z) == 3
                ix(end+1)=x;
                iy(end+1)=y;
                iz(end+1)=z;
                %             data2(x,y,z) =  data(x,y,z);
%                  box(x,y,z);


            else
%                 data2(x,y,z) =0;
            end
        end
    end
end
%%
scatter3(ix,iy,iz, 2);

% contourslice(data2,[],[],1:10:100,1)
%%

data2=zeros(xdim,ydim,zdim);
deg=zeros(1,30);

for z=1:zdim
    for y=1:ydim
        for x=1:xdim
            if data(x,y,z)==label
                tmp=0;
                for dz=-1:1
                    for dy=-1:1
                        for dx=-1:1
                            if (x+dx)>0 && (x+dx)<=xdim && (y+dy)>0 && (y+dy)<=ydim && (z+dz)>0 && (z+dz)<=zdim
                                if ~(dx==0 && dy==0 && dz==0) && data(x+dx,y+dy,z+dz)==label
                                    tmp=tmp+1;
                                end
                            end
                        end
                    end
                end
                data2(x,y,z)=tmp;
                deg(tmp)=deg(tmp)+1;
            end
            
        end
    end
end
                            

            
