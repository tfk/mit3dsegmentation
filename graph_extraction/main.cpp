// Copyright 2014

#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <opencv2/core/core.hpp>
//#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cilk/cilk.h>
#include <cilk/reducer.h>
#include <cilk/reducer_opadd.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstdio>

#include <string>
#include <set>
#include <queue>

#include "../tfk_utils.h"
#include "../parallel_utils.h"
#include "../vector_reducer.h"
#include "./graph_properties.h"  // Includes vdata/edata and height/cols/rows.
#include "../imageGraphIO.h"
#include "../Graph.h"
#include "../engine.h"
#include "../scheduler.h"

#include "../Graph.cpp"
#include "../engine.cpp"
#include "../scheduler.cpp"

// Path to ith input image is:
// INPUT_IMAGE_DIR + "/" + INPUT_IMAGE_FILENAME_PREFIX + "i" +
//       INPUT_IMAGE_FILENAME_EXT
static std::string INPUT_IMAGE_DIR =
    "/scratch/segmentation_directory/grayscale_input1";
//   "testInput/grayscale_input1";

static std::string INPUT_IMAGE_FILENAME_PREFIX = "input-image";
static std::string INPUT_IMAGE_FILENAME_EXT = ".tif";

static std::string LABELED_IMAGE_DIR =
    "/scratch/segmentation_directory/labels_input1";
//    "testInput/labels_input1";

static std::string LABELED_IMAGE_FILENAME_PREFIX =
    "input-labels-";
static std::string LABELED_IMAGE_FILENAME_EXT = ".tif";

static int OPENCV_LABEL_TYPE = CV_16U;
typedef uint16_t LABEL_TYPE;

// Path format for output files
//   Will create images with .jpg extension suitable for human viewing,
//   and .tif extension for algorithmic comparison with ground truth.
static std::string OUTPUT_IMAGE_DIR = "/afs/csail.mit.edu/u/o/odor/public_html/graph_extraction_output";
static std::string OUTPUT_IMAGE_FILENAME_PREFIX = "output-stack-label";

LABEL_TYPE** labels_array;

bool* simple;

void get_object_label_update(int vid,
                     void* scheduler_void) {
  int x = graph->getVertexData(vid)->x;
  int y = graph->getVertexData(vid)->y;
  int z = graph->getVertexData(vid)->z;
  graph->getVertexData(vid)->object_label = labels_array[z][_cols*y + x];
  graph->getVertexData(vid)->deleted = false;
  graph->getVertexData(vid)->checked = 0;
}

void peel(int vid, void* scheduler_void);
bool simplePoint(int vid);

void peelInit(int vid, void* scheduler_void) {
   Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
   std::vector<int> neighbors;
   graph->getManGridEdges(vid, &neighbors); //Manhattan distance

   int z= graph->getVertexData(vid)->z;

  bool onSurface=false;
  if (graph->getVertexData(vid)->object_label>0) 
     for (int i = 0; i < neighbors.size(); i++) 
           if(graph->getVertexData(vid)->object_label>0 && graph->getVertexData(neighbors[i])->object_label!=graph->getVertexData(vid)->object_label) 
           	if (graph->getVertexData(neighbors[i])->z==z)  
	   		onSurface=true;
  if (onSurface) 
    scheduler->add_task(vid , peel);
}


void peel(int vid, void* scheduler_void) {
   if (graph->getVertexData(vid)->deleted ) return;
   Scheduler* scheduler = reinterpret_cast<Scheduler*>(scheduler_void);
   std::vector<int> neighbors;
   graph->getCube(vid, 1, &neighbors);

   if (simplePoint(vid) && neighbors.size()>1) {
      graph->getVertexData(vid)->deleted = true;
      for (int i = 0; i < neighbors.size(); i++) {
	    graph->getVertexData(neighbors[i])->roundNum=graph->getVertexData(vid)->checked+graph->getVertexData(vid)->roundNum+1;
	    scheduler->add_task( neighbors[i] , peel);
      }
   }
   else {
       graph->getVertexData(vid)->checked+=1;
       if (graph->getVertexData(vid)->checked<10)
           scheduler->add_task( vid , peel);
    }

}

bool simplePoint(int vid) {

    int x = graph->getVertexData(vid)->x;
    int y = graph->getVertexData(vid)->y;
    int z = graph->getVertexData(vid)->z;

   int ex=-1; //exponent starting from 3^3-1
   int val=0; //hash value
   int rows=graph->_rows;
   int cols=graph->_cols;
   int height= graph->_height;
   for(int dx=-1; dx < 2; dx++) {  
       for(int dy=-1; dy<2; dy++) {
	   for(int dz=-1; dz<2; dz++) {
	      ex++;
	      if (y+dy < 0 || y+dy >= rows) continue;
	      if (x+dx < 0 || x+dx >= cols) continue;
	      if (z+dz < 0 || z+dz >= height) continue;
	      int pixel = (z+dz)*rows*cols + (y+dy)*graph->_cols + x+dx;
	      if (graph->getVertexData(vid)->object_label==graph->getVertexData(pixel)->object_label)
		if (!graph->getVertexData(pixel)->deleted) {
		  val+=pow(2,ex);
	//	  printf("%d %d %d\n",dx+1,dy+1,dz+1);
		}
	   }
       }
   }
  if(val>pow(2,27)) printf("Simple overflow");
//  printf("Simple finish %d %d\n", vid,val);
  return simple[val];
}


// Input: Grayscale image stack + Object-Labeled image stack
// Output: Graph with objects as vertices and interactions as edges.
int main(int argc, char **argv) {
  //printf("size of vdata %d\n", sizeof(vdata));
  //return 0;
  int rows = _rows;
  int cols = _cols;
  int height = _height;

  // arg 1: rand seed
  // arg 2: grayscale image directory
  // arg 3: labeled image directory
  if (argc > 1) {
    std::string argstring(argv[1]);
    int seed_arg = StringToNumber<int>(argstring);
    srand(seed_arg);
  } else {
    srand(0);
  }

  // If input directories are specified, then overwrite defaults.
  if (argc > 3) {
    std::string grayscale_image_directory(argv[2]);
    std::string labeled_image_directory(argv[3]);
    INPUT_IMAGE_DIR = grayscale_image_directory;
    LABELED_IMAGE_DIR = labeled_image_directory;
  }

  labels_array =
     static_cast<uint16_t**>(malloc(sizeof(uint16_t*)*height));
  uint8_t** input_image_stack =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc1 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc2 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));
  uint8_t** input_image_stackc3 =
      static_cast<uint8_t**>(malloc(sizeof(uint8_t*)*height));

  // load the image stack
  for (int i = 0; i < height; i++) {
    // Load ith level of grayscale stack.
    cv::Mat im;
    std::string grayscale_path = INPUT_IMAGE_DIR + "/" +
	INPUT_IMAGE_FILENAME_PREFIX + NumberToString(i) +
	INPUT_IMAGE_FILENAME_EXT;
    printf("Grayscale path is %s\n", grayscale_path.c_str());
    im = cv::imread(grayscale_path.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
    im.convertTo(im, CV_8U);
    printf("Success dims %d %d\n", im.dims, i);
    input_image_stack[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc1[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc2[i] = Mat_to_Row_Major<uint8_t>(im);
    input_image_stackc3[i] = Mat_to_Row_Major<uint8_t>(im);


    // Load ith level of label stack.
    std::string labels_path = LABELED_IMAGE_DIR + "/" +
	LABELED_IMAGE_FILENAME_PREFIX + NumberToString(i+1) +
	LABELED_IMAGE_FILENAME_EXT;
    cv::Mat im2;
    im2 = cv::imread(labels_path.c_str(), CV_LOAD_IMAGE_UNCHANGED);
    printf("Labels path is %s\n", labels_path.c_str());
    im2.convertTo(im2, OPENCV_LABEL_TYPE);
    printf("Success labels dims %d %d\n", im2.dims, i);
    labels_array[i] = Mat_to_Row_Major<LABEL_TYPE>(im2, OPENCV_LABEL_TYPE);
  }


  uint16_t max = 0;
  for (int z = 0; z < height; z++) {
    for (int xy = 0; xy < rows*cols; xy++) {
      if (labels_array[z][xy] > max) max = labels_array[z][xy];
    }
  }
  printf("Max label is %d\n", max);

  // Modify the graph_properties.h file local to graph_extraction directory
  //   to add additional fields to vdata structure.
  graph = row_major_to_graph(input_image_stack, rows, cols, height);

  // Simple DG-Computation to populate the object_label field of vdata
  // This could be done by using an alternate row_major_to_graph function, but
  //   we'll do it this way since it also illustrates how to do DG computations

  //Reading LUT
  int N=pow(2,pow(3,3));
  simple = (bool*) malloc (N*sizeof(bool));
  FILE* in = fopen("save1029_LUT.txt","r");
  int sum=0;
  for(int i=0; i<N; i++) {
	char temp;
	fscanf(in,"%c",&temp);
	if (temp=='0')
	   simple[i]=0;
	else if (temp=='1')
	   simple[i]=1;
	else
	   printf("ERROR, %d",temp);
	if (simple[i]) sum++;
  }	
  fclose(in);
  printf("LUT file read. %d simple points detected\n",sum );


  // Compute "trivial" size 1 coloring
  // int colorCount = graph->compute_trivial_coloring();
  // Replace line above with commented out line below for distance 1 coloring

  int colorCount = graph->compute_coloring_grid(_rows, _cols, _height);

  scheduler = new Scheduler(graph->vertexColors, colorCount,
      graph->num_vertices());
  e = new engine<vdata, edata>(graph, scheduler);

  cilk_for (int xyz = 0; xyz < height*rows*cols; xyz++) {
    scheduler->add_task(xyz, get_object_label_update);
  }
  e->run();

  cilk_for (int xyz = 0; xyz < height*rows*cols; xyz++) {
    scheduler->add_task(xyz, peelInit);
  }
  e->run();

  cilk::reducer_opadd<int> remains_reducer(0);
  cilk::reducer_opadd<int> maxRound_reducer(0);
  // Check that the object labels in graph match the array.
  cilk_for (int z = 0; z < height; z++) {
    cilk_for (int xy = 0; xy < rows*cols; xy++) {
      int vid = z*rows*cols + xy;
      if(!graph->getVertexData(vid)->deleted && graph->getVertexData(vid)->object_label>0) {
	 remains_reducer++;
      } 
      if (graph->getVertexData(vid)->roundNum>maxRound_reducer.get_value())
	 maxRound_reducer+=graph->getVertexData(vid)->roundNum-maxRound_reducer.get_value();

      assert(graph->getVertexData(vid)->object_label == labels_array[z][xy]);
    }
  }
  int remains = remains_reducer.get_value();
  int maxRound=maxRound_reducer.get_value();
  printf("Remaining nodes %d\n", remains);
  printf("Number of rounds %d\n", maxRound);


  // Get max and min label (using reducers for illustration.
  cilk::reducer_max<int> max_label;
  cilk::reducer_min<int> min_label;
  // The following code is just a test that the object labels were grabbed.
  cilk_for (int xyz = 0; xyz < rows*cols*height; xyz++) {
	int x = graph->getVertexData(xyz)->x;
	int y = graph->getVertexData(xyz)->y;
	int z = graph->getVertexData(xyz)->z;
	assert(graph->getVertexData(xyz)->object_label ==
	    labels_array[z][y*cols + x]);
	max_label.calc_max(graph->getVertexData(xyz)->object_label);
	min_label.calc_min(graph->getVertexData(xyz)->object_label);
  }
  printf("The max label is %d, min label is %d\n",
      max_label.get_value(), min_label.get_value());


//for(int kz=0; kz<1; kz++) {

  // lets count the distinct labels.
  uint16_t* label_array =
      static_cast<uint16_t*>(calloc(_height*_rows*_cols, sizeof(uint16_t)));
  uint8_t* label_array_c1 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  uint8_t* label_array_c2 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  uint8_t* label_array_c3 =
      static_cast<uint8_t*>(calloc(_height*_rows*_cols, sizeof(uint8_t)));
  int* label_sizes =
      static_cast<int*>(calloc(_height*_rows*_cols, sizeof(int)));

  printf("Assigning a color for each distinct label.\n");

  // eliminate labels that are too small.
  cilk_for (int z = 0; z < _height; z++) {
    cilk_for (int i = 0; i < _rows*_cols; i++) {
      int pixel = z*_rows*_cols + i;
      if (graph->getVertexData(pixel)->object_label > 0) {
	int label = graph->getVertexData(pixel)->object_label;
	__sync_fetch_and_add(&label_sizes[label], 1);
      }
    }
  }

  int distinct_labels = 0;
  for (int v = 0; v < _height*_rows*_cols; v++) {
    int label = graph->getVertexData(v)->object_label;
    if (label_array[label] == 0 &&
	graph->getVertexData(v)->object_label > 0) {
      if (label_sizes[label] >= 1) {
	label_array_c1[label] = (uint8_t)rand();
	label_array_c2[label] = (uint8_t)rand();
	label_array_c3[label] = (uint8_t)rand();
	distinct_labels++;
	label_array[label] = distinct_labels;
      }
    }
  }
  printf("Number of distinct labels %d\n", distinct_labels);
  printf("Coloring the graph \n");

  cilk_for (int z = 0; z <_height; z++) {
    cilk_for (int i = 0; i < _rows*_cols; i++) {
      int pixel = z*_rows*_cols + i;
//	std::cout << graph->getVertexData(pixel)->roundNum<<"\n";
 //      if ((graph->getVertexData(pixel)->parent==pixel) && graph->getVertexData(pixel)->object_label > 0) { //    if ((graph->getVertexData(pixel)->roundNum==0) && graph->getVertexData(pixel)->deleted && graph->getVertexData(pixel)->object_label > 0) {
//    if ((graph->getVertexData(pixel)->roundNum<=kz) && graph->getVertexData(pixel)->deleted && graph->getVertexData(pixel)->object_label > 0) {
//    if ((graph->getVertexData(pixel)->roundNum==kz) && graph->getVertexData(pixel)->object_label > 0) {
   if ((!graph->getVertexData(pixel)->deleted)  && graph->getVertexData(pixel)->object_label > 0) {
//      if ((graph->getVertexData(pixel)->deleted)  && graph->getVertexData(pixel)->object_label > 0) {
//     if ((graph->getVertexData(pixel)->localmax)  && graph->getVertexData(pixel)->object_label > 0) {
//     if (graph->getVertexData(pixel)->object_label==10) {
	int label = graph->getVertexData(pixel)->object_label;
	input_image_stackc1[z][i] = label_array_c1[label];
	input_image_stackc2[z][i] = label_array_c2[label];
	input_image_stackc3[z][i] = label_array_c3[label];
      }
      else
      {
	input_image_stackc1[z][i] = 0;
	input_image_stackc2[z][i] = 0;
	input_image_stackc3[z][i] = 0;
      }
    }
  }

  cilk_for (int i = 0; i < height; i++) {
    cv::Mat tmpc1 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc1[i]);
    cv::Mat tmpc2 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc2[i]);
    cv::Mat tmpc3 = cv::Mat(_rows, _cols, CV_8UC1, input_image_stackc3[i]);
    std::string path = OUTPUT_IMAGE_DIR + "/" + OUTPUT_IMAGE_FILENAME_PREFIX +
	"-" + NumberToString(i) + ".jpg";
    std::string pathtif = OUTPUT_IMAGE_DIR + "/" +
	OUTPUT_IMAGE_FILENAME_PREFIX + "-" +
	NumberToString(i) + ".tif";
    cv::Mat tmp = colorize(tmpc1, tmpc2, tmpc3);
    std::cout<<path.c_str();
  }


printf("DONE!");
//saving data for matlab
/*
  printf("Saving extracted data to file");
  std::string filename=OUTPUT_IMAGE_DIR + "/" + OUTPUT_IMAGE_FILENAME_PREFIX + ".yml";
  FILE* myfile;
  myfile= fopen(filename.c_str(),"w");
  for (int z = 0; z < _height; z++) {
    for (int y = 0; y < _rows; y++) {
	 for (int x = 0; x < _cols; x++) {
	     int pixel = z*_rows*_cols + y*_cols + x;
	     if (graph->getVertexData(pixel)->deleted)
		     fprintf(myfile,"0 ");
	     else
		     fprintf(myfile,"%d ",graph->getVertexData(pixel)->object_label);
	 }
	 fprintf(myfile,"\n");
    }
    fprintf(myfile,"\n");
  }

  fclose(myfile);*/

  return 0;
}

