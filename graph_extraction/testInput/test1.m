scale=16;
%image=uint16(zeros(1024/scale,1024/scale,100));

for z=1:100
    image=uint16(zeros(1024/scale,1024/scale));
    if z<=50
        for x=1:1024/scale
            for y=1:1024/scale                
                %object 1: cylinder with radius 64
                origin=[256,256,0]/scale;
                R=64/scale;
                ox=x-origin(1);
                oy=y-origin(2);

                if ((ox)^2+(oy)^2<=(R)^2)
                    image(x,y)=1;
                    continue;
                end
                
                %object 2: Right Square Prism with side 64
                origin=[768,256,0]/scale;
                R=64/scale;
                ox=x-origin(1);
                oy=y-origin(2);
                
                if (abs(ox)<=R && abs(oy)<=R)
                    image(x,y)=2;
                    continue;
                end
                
                %object 3: branching cylinder
                origin=[256,768,25*scale]/scale;
                R=64/scale;
                ox=x-origin(1);
                oy=y-origin(2);
                oz=z-origin(3);

                %upper half
                if ((ox)^2+(oy)^2<=(R)^2) && oz<=0
                    image(x,y)=3;
                    continue;
                end
                
                done=false;
                %lower half
                R=32/scale;
                theta=pi/4;
                
                for i=-1:2:1
                    rot=[cos(i*theta) 0 sin(i*theta); 0 1 0; -sin(i*theta) 0 cos(i*theta)];
                    rotVect = rot*[ox,oy,oz]';
                    
                    if ((rotVect(1))^2+(rotVect(2))^2<=(R)^2) && rotVect(3)>=0 && rotVect(3)<=20
                        image(x,y)=3;
                        done=true;
                    end
                end
                
                if done
                    continue;
                end
                
                %object 4: Right Square Prism with side 64
                
                origin=[768,768,25*scale]/scale;
                R=64/scale;
                ox=x-origin(1);
                oy=y-origin(2);
                oz=z-origin(3);

                %upper half
                if abs(ox)<=(R) && abs(oy)<=(R) && oz<=0
                    image(x,y)=4;
                end
                
                %lower half
                R=32/scale;
                theta=pi/4;
                
                for i=-1:2:1
                    rot=[cos(i*theta) 0 sin(i*theta); 0 1 0; -sin(i*theta) 0 cos(i*theta)];
                    rotVect = rot*[ox,oy,oz]';
                    
                    if abs(rotVect(1))<=(R) && abs(rotVect(2))<=(R) && rotVect(3)>=0 && rotVect(3)<=20
                        image(x,y)=4;
                    end
                end
                
                
                
            end
        end
        
    end
    
    imwrite(image,['labels_input1/input-labels-' num2str(z) '.tif']);
    disp(z)
    
end



