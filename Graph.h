// Copyright 2014

#include <stdio.h>
#include <cilk/cilk.h>
#include <cilk/reducer_list.h>
#include <cilk/reducer_min.h>
#include <cilk/reducer_max.h>
#include <cilk/holder.h>

#include <vector>
#include <cmath>
#include <list>
#include <map>
#include <string>
#include <set>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#ifndef GRAPH_H_
#define GRAPH_H_

typedef struct edge_info {
  int edge_id;
  int out_vertex;
  int in_vertex;
  int next;
} edge_info;

template<typename VertexType, typename EdgeType>
class Graph {
 private:
    int nextEdgeId;
    std::vector<struct edge_info> temp_edges;
    VertexType* vertexData;
    EdgeType* edgeData;
    int* outDegree;
    int* inDegree;

    // maps vertexId to start of out edges.
    int* out_edge_index;
    int* in_edge_index;

    struct edge_info* out_edges;
    struct edge_info* in_edges;

 public:
    Graph();
    int* vertexColors;
    int vertexCount;
    int compute_trivial_coloring();
    int compute_coloring_grid(int rows, int cols, int height);
    void addEdge(int vid1, int vid2, EdgeType edgeInfo);
    void addVertex(int vid, VertexType vdata);
    void partition(int v, int* order, int* partitionIndexIn,
        int* partitionIndexOut);
    bool updateIndices(int r, int v, int* order,
        int* partitionIndexIn, int* partitionIndexOut, int* currentIndexIn,
        int* currentIndexOut, int* currentIndexInDynamic,
        int* currentIndexOutDynamic);
    void colorVertex(int v);
    void asyncColor(int v, int* order, int* counters,
        cilk::holder< std::set<int> >* neighbor_set_holder);
    void finalize();
    void resize(int size);
    void prefetch_vertex(int vid);
    int compute_coloring();
    int compute_coloring_rootset();
    int compute_coloring_atomiccounter();
    void validate_coloring();
    int getVertexColor(int vid);
    int getOutDegree(int vid);
    int getInDegree(int vid);
    int num_vertices();
    int num_edges();
    struct edge_info* getOutEdges(int vid);
    struct edge_info* getInEdges(int vid);
    VertexType* getVertexData(int vid);
    EdgeType* getEdgeData(int eid);
    void setGridDimensions(int rows, int cols, int height);
    void getManGridEdges(int vid, std::vector<int>* b);
    void getGridEdges(int vid, std::vector<int>* b);
    void get2DGridEdges(int vid, std::vector<int>* b, int distanceSq);
    void getCube(int vid, int radius,  std::vector<int>* b);
    void getManCube(int vid, int radius,  std::vector<int>* b);
    int _height;
    int _rows;
    int _cols;
};

#endif  // GRAPH_H_
