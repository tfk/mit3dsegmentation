#include "Graph.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

void paged_free(void* p) {
  uint64_t* b = ((uint64_t*)p)-2;
  FILE* t = (FILE*)b[0];
  uint64_t size = b[1];
  munmap(b, size);
  fclose(t);
}

void* paged_malloc(uint64_t size) {
  FILE* f = tmpfile();
  int t = fileno(f);
  size = size+sizeof(uint64_t)*2;
  fallocate(t, 0, 0, size);
  uint64_t* p = (uint64_t*) mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, t, 0);
  p[0] = (uint64_t)f;
  p[1] = size;
  return (void*)(p+2);
}

//Non-deleted bject points in a cube
template<typename VertexType, typename EdgeType>
Graph<VertexType, EdgeType>::Graph() {
  nextEdgeId = 0;
  vertexData = NULL;
  edgeData = NULL;
}

template<typename VertexType, typename EdgeType>
void Graph< VertexType,  EdgeType>::addEdge(int vid1, int vid2, EdgeType edgeInfo){

  // Grow the edgeData array. This should probably be replaced with a vector.
  if (nextEdgeId%10000 == 0) {
    edgeData = (EdgeType*) realloc(edgeData, (nextEdgeId+10000)*sizeof(EdgeType));
  }
  edgeData[nextEdgeId] = edgeInfo;
  struct edge_info edge;
  edge.edge_id = nextEdgeId;
  edge.out_vertex = vid1;
  edge.in_vertex = vid2;
  temp_edges.push_back(edge);
  nextEdgeId++;
}

//Non-deleted bject points with manhattan distace
template <typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::getManCube(int vid, int radius,  std::vector<int>* b) {
  VertexType* vd = this->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  for (int dx = -radius; dx <= radius; dx++) {
    for (int dy = -radius; dy <= radius; dy++) {
      for (int dz = -radius; dz <= radius; dz++) {
        if (y+dy < 0 || y+dy >= rows) continue;
        if (x+dx < 0 || x+dx >= cols) continue;
        if (z+dz < 0 || z+dz >= height) continue;
	if (abs(dx)+abs(dy)+abs(dz)>radius) continue;
	int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
        if (vid == pixel) continue;
	if (vd->object_label==this->getVertexData(pixel)->object_label) {
	   if (!this->getVertexData(pixel)->deleted) {
       		 b->push_back(pixel);
           }
	}
      }
    }
  }
}

//Non-deleted bject points in a cube
template <typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::getCube(int vid, int radius,  std::vector<int>* b) {
  VertexType* vd = this->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  for (int dx = -radius; dx <= radius; dx++) {
    for (int dy = -radius; dy <= radius; dy++) {
      for (int dz = -radius; dz <= radius; dz++) {
        if (y+dy < 0 || y+dy >= rows) continue;
        if (x+dx < 0 || x+dx >= cols) continue;
        if (z+dz < 0 || z+dz >= height) continue;
	int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
        if (vid == pixel) continue;
	if (vd->object_label==this->getVertexData(pixel)->object_label) {
	   if (!this->getVertexData(pixel)->deleted) {
       		 b->push_back(pixel);
           }
	}
      }
    }
  }
}


template<typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::addVertex(int vid, VertexType vdata){
  assert(vertexCount > vid);
  vertexData[vid] = vdata;
}

template<typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::resize(int size){
  vertexCount = size;
  uint64_t new_size = size * sizeof(VertexType);
  //vertexData = (VertexType*) paged_malloc(new_size);
  vertexData = (VertexType*) realloc(vertexData, vertexCount * sizeof(VertexType));
}

template<typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::finalize(){
  out_edges = (struct edge_info*) calloc(nextEdgeId, sizeof(struct edge_info));
  in_edges = (struct edge_info*) calloc(nextEdgeId, sizeof(struct edge_info));

  out_edge_index = (int*) calloc(nextEdgeId, sizeof(int));
  in_edge_index = (int*) calloc(nextEdgeId, sizeof(int));

  outDegree = (int*) calloc(vertexCount, sizeof(int));
  inDegree = (int*) calloc(vertexCount, sizeof(int));

  // compute the in degree and out degrees.
  for (int i = 0; i < temp_edges.size(); i++) {
    outDegree[temp_edges[i].out_vertex] += 1;
    inDegree[temp_edges[i].in_vertex] += 1;
  }

  // lets compute the out_edge and in_edge indices.
  out_edge_index[0] = 0;
  in_edge_index[0] = 0;
  for (int i = 1; i < vertexCount; i++) {
    out_edge_index[i] = out_edge_index[i-1] + outDegree[i-1];
    in_edge_index[i] = in_edge_index[i-1] + inDegree[i-1];
  }

  // now put all the edges into position.
  for (int i = 0; i < temp_edges.size(); i++) {
    int out_index = out_edge_index[temp_edges[i].out_vertex]++;
    int in_index = in_edge_index[temp_edges[i].in_vertex]++;
    out_edges[out_index] = temp_edges[i];
    in_edges[in_index] = temp_edges[i];
  }

  // now we need to subtract the degrees from the indicies.
  for (int i = 0; i < vertexCount; i++) {
    out_edge_index[i] = out_edge_index[i] - outDegree[i];
    in_edge_index[i] = in_edge_index[i] - inDegree[i];
  }
}

template<typename VertexType, typename EdgeType>
struct edge_info* Graph< VertexType,  EdgeType>::getOutEdges(int vid){
  return &out_edges[out_edge_index[vid]];
}

template<typename VertexType, typename EdgeType>
struct edge_info* Graph< VertexType,  EdgeType>::getInEdges(int vid){
  return &in_edges[in_edge_index[vid]];
}

template<typename VertexType, typename EdgeType>
VertexType* Graph< VertexType,  EdgeType>::getVertexData(int vid){
  return &vertexData[vid];
}

template<typename VertexType, typename EdgeType>
EdgeType* Graph< VertexType,  EdgeType>::getEdgeData(int eid){
  return &edgeData[eid];
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::getVertexColor(int vid){
  return vertexColors[vid];
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::getOutDegree(int vid){
  return outDegree[vid];
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::getInDegree(int vid){
  return inDegree[vid];
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::num_vertices(){
  return vertexCount;
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::num_edges(){
  return nextEdgeId;
}

template<typename VertexType, typename EdgeType>
void Graph< VertexType, EdgeType>::validate_coloring() {
  for (int v = 0; v < vertexCount; v++) {
    struct edge_info* inEdges = getInEdges(v);
    struct edge_info* outEdges = getOutEdges(v);

    for (int i = 0; i < inDegree[v]; i++) {
      if (getVertexColor(inEdges[i].out_vertex) == getVertexColor(v)) {
        printf("Invalid coloring \n");
        return;
      }
    }
    for (int i = 0; i < outDegree[v]; i++) {
      if (getVertexColor(outEdges[i].in_vertex) == getVertexColor(v)) {
        printf("Invalid coloring \n");
        return;
      }
    }
  }
  printf("Valid coloring \n");
}

// returns true if this vertex is in the first root set.
template<typename VertexType, typename EdgeType>
void Graph< VertexType, EdgeType>::partition(int v, int* order, int* partitionIndexIn, int* partitionIndexOut) {
  struct edge_info* inEdges = getInEdges(v);
  struct edge_info* outEdges = getOutEdges(v);

  int j = 0;
  for (int i = 0; i < inDegree[v]; i++) {
    if (order[inEdges[i].out_vertex] < order[v]) {
      struct edge_info tmp = inEdges[j];
      inEdges[j] = inEdges[i];
      inEdges[i] = tmp;
      j++;
    }
  }
  partitionIndexIn[v] = j;

  j = 0;
  for (int i = 0; i < outDegree[v]; i++) {
    if (order[outEdges[i].in_vertex] < order[v]) {
      struct edge_info tmp = outEdges[j];
      outEdges[j] = outEdges[i];;
      outEdges[i] = tmp;
      j++;
    }
  }
  partitionIndexOut[v] = j;
}


template<typename VertexType, typename EdgeType>
void Graph< VertexType, EdgeType>::colorVertex(int v) {
  std::set<int> neighbor_colors;
  struct edge_info * inEdges = getInEdges(v);
  struct edge_info * outEdges = getOutEdges(v);
  for (int i = 0; i < inDegree[v]; i++) {
    int u = inEdges[i].out_vertex;
    neighbor_colors.insert(getVertexColor(u));
  }
  for (int i = 0; i < outDegree[v]; i++) {
    int u = outEdges[i].in_vertex;
    neighbor_colors.insert(getVertexColor(u));
  }
  int color = 0;
  while (neighbor_colors.find(color) != neighbor_colors.end()) {
    color++;
  }
  vertexColors[v] = color;
}

void radix_sort(int* colors, int size, int radix) {
  // Partition so that everything with a 0 bit is in left.
  int j = 0;
  for (int i = 0; i < size; i++) {
    if (!(colors[i] & (1<<(radix-1)))) {
      int tmp = colors[j];
      colors[j] = colors[i];
      colors[i] = tmp;
      j++;
    }
  }

  if (radix > 0) {
    radix_sort(colors, j, radix - 1);
    radix_sort(colors + j, size - j, radix - 1);
  }
}

template<typename VertexType, typename EdgeType>
void Graph< VertexType, EdgeType>::asyncColor(int v, int* order, int* counters,
    cilk::holder< std::set<int> >* neighbor_set_holder)  {
  std::set<int>& neighbor_colors = (*neighbor_set_holder)();
  neighbor_colors.clear();

  struct edge_info * inEdges = getInEdges(v);
  struct edge_info * outEdges = getOutEdges(v);

  int* neighborColors = (int*) calloc(sizeof(int),inDegree[v] + outDegree[v]);

  cilk_for(int i = 0; i < inDegree[v] + outDegree[v]; i++) {
    int u;
    if (i < inDegree[v]) {
      u = inEdges[i].out_vertex;
    } else {
      u = outEdges[i - inDegree[v]].in_vertex;
    }
    int color = vertexColors[u];
    if (order[u] < order[v] && color < inDegree[v] + outDegree[v]) {
      neighborColors[color] = 1;
    }
  }

  int color = -1;
  for (int i = 0; i < inDegree[v] + outDegree[v]; i++) {
    if (neighborColors[i] == 0) {
      color = i;
      break;
    }
  }
  if (color == -1) {
    color = inDegree[v] + outDegree[v];
  }
  vertexColors[v] = color;
  // decrement all bigger neighbors
  cilk_for (int i = 0; i < inDegree[v] + outDegree[v]; i++) {
    int u;
    if (i < inDegree[v]) {
      u = inEdges[i].out_vertex;
    } else {
      u = outEdges[i - inDegree[v]].in_vertex;
    }
    if (order[u] > order[v]) {
      __sync_fetch_and_sub(&counters[u], 1);
      if (__sync_bool_compare_and_swap(&counters[u], 0, -1)) {
        asyncColor(u, order, counters, neighbor_set_holder);
      }
    }
  }
}

template<typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::setGridDimensions(int rows, int cols, int height) {
  this->_rows = rows;
  this->_cols = cols;
  this->_height = height;
}

// Returns all pixels within distanceSq squared distance from vid.
template <typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::get2DGridEdges(int vid, std::vector<int>* b,
    int distanceSq) {
  VertexType* vd = this->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  int distance = sqrt(distanceSq);
  for (int dx = -distance; dx <= distance; dx++) {
    for (int dy = -distance; dy <= distance; dy++) {
      if (y+dy < 0 || y+dy >= rows) continue;
      if (x+dx < 0 || x+dx >= cols) continue;
      if (dx*dx+dy*dy > distanceSq) continue;
      int pixel = vid + (dy)*_cols + dx;
      if (vid == pixel) continue;
      b->push_back(pixel);
    }
  }
}

//Manhattan distance
template <typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::getManGridEdges(int vid, std::vector<int>* b) {
  VertexType* vd = this->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      for (int dz = -1; dz <= 1; dz++) {
        if (y+dy < 0 || y+dy >= rows) continue;
        if (x+dx < 0 || x+dx >= cols) continue;
        if (z+dz < 0 || z+dz >= _height) continue;
	if (abs(dx)+abs(dy)+abs(dz)>1) continue;
        int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
        if (vid == pixel) continue;
        b->push_back(pixel);
      }
    }
  }
}

//Diagonals too
template <typename VertexType, typename EdgeType>
void Graph<VertexType, EdgeType>::getGridEdges(int vid, std::vector<int>* b) {
  VertexType* vd = this->getVertexData(vid);
  int x = vd->x;
  int y = vd->y;
  int z = vd->z;
  int rows = _rows;
  int cols = _cols;
  int height = _height;
  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      for (int dz = -1; dz <= 1; dz++) {
        if (y+dy < 0 || y+dy >= rows) continue;
        if (x+dx < 0 || x+dx >= cols) continue;
        if (z+dz < 0 || z+dz >= _height) continue;
        int pixel = (z+dz)*_rows*_cols + (y+dy)*_cols + x+dx;
        if (vid == pixel) continue;
        b->push_back(pixel);
      }
    }
  }
}

template<typename VertexType, typename EdgeType>
bool Graph< VertexType, EdgeType>::updateIndices(int r, int v, int* order,
    int* partitionIndexIn, int* partitionIndexOut, int* currentIndexIn,
    int* currentIndexOut, int* currentIndexInDynamic, int* currentIndexOutDynamic) {
  struct edge_info* inEdges = getInEdges(v);
  struct edge_info* outEdges = getOutEdges(v);

  if (currentIndexIn[v] < partitionIndexIn[v]) {
    if (r != inEdges[currentIndexIn[v]].out_vertex) {
      return false;
    }
  } else {
    if (r != outEdges[currentIndexOut[v]].in_vertex) {
      return false;
    }
  }

  while (currentIndexInDynamic[v] < partitionIndexIn[v]) {
    int index = currentIndexInDynamic[v];
    int neighbor = inEdges[index].out_vertex;
    if (vertexColors[neighbor] == -1) {
      break;
    }
    currentIndexInDynamic[v]++;
  }
  currentIndexIn[v] = currentIndexInDynamic[v];

  while (currentIndexOutDynamic[v] < partitionIndexOut[v]) {
    int index = currentIndexOutDynamic[v];
    int neighbor = outEdges[index].in_vertex;
    if (vertexColors[neighbor] == -1) {
      break;
    }
    currentIndexOutDynamic[v]++;
  }
  currentIndexOut[v] = currentIndexOutDynamic[v];

  return true;
}

// Uses the root set method to compute a valid coloring.
template<typename VertexType, typename EdgeType>
int Graph< VertexType, EdgeType>::compute_coloring_rootset() {

  // Step 1: Give the vertices an order.
  std::vector<std::pair<int, int> > permutation(vertexCount);
  cilk_for(int v = 0; v < vertexCount; v++) {
    permutation[v] = std::make_pair(-(inDegree[v] + outDegree[v]), v);
  }
  std::sort(permutation.begin(), permutation.end());

  int* order = (int*) malloc(sizeof(int) * vertexCount);
  int* partitionIndexIn = (int*) calloc(sizeof(int), vertexCount);
  int* partitionIndexOut = (int*) calloc(sizeof(int), vertexCount);

  int* currentIndexIn = (int*) calloc(sizeof(int), vertexCount);
  int* currentIndexOut = (int*) calloc(sizeof(int), vertexCount);

  int* currentIndexInDynamic = (int*) calloc(sizeof(int), vertexCount);
  int* currentIndexOutDynamic = (int*) calloc(sizeof(int), vertexCount);

  vertexColors = (int*) malloc(sizeof(int) * vertexCount);

  cilk_for (int i = 0; i < vertexCount; i++) {
    order[permutation[i].second] = i;
    vertexColors[i] = -1;
  }

  // Step 2: Partition the adjacency lists of each vertex.
  cilk_for (int v = 0; v < vertexCount; v++) {
    partition(v, order, partitionIndexIn, partitionIndexOut);
  }

  int rootSetCount = 0;
  std::vector<int> rootSet1;
  std::vector<int> rootSet2;
  std::vector<int> * rootSet = &rootSet1;
  std::vector<int> * newRootSet = &rootSet2;
  // Step 3: Identify the root sets.
  for (int v = 0; v < vertexCount; v++) {
    if (partitionIndexIn[v] == 0 && partitionIndexOut[v] == 0) {
      rootSet->push_back(v);
    }
  }

  int iterationCount = 0;
  while (rootSet->size() > 0) {
    rootSetCount++;
    newRootSet->clear();
    //printf("Root set size %d\n", (int)rootSet->size());
    // Color the root set.
    for (int i = 0; i < rootSet->size(); i++) {
      colorVertex((*rootSet)[i]);
    }
    // Update the currentIndexIn and currentIndexOut
    for (int i = 0; i < rootSet->size(); i++) {
      int r = (*rootSet)[i];
      for (int j = 0; j < inDegree[r]; j++) {
        int v = getInEdges(r)[j].out_vertex;
        if (vertexColors[v] != -1) {
          continue;
        }
        bool processed = updateIndices(r, v, order, partitionIndexIn, partitionIndexOut,
            currentIndexIn, currentIndexOut, currentIndexInDynamic, currentIndexOutDynamic);
        if (processed && currentIndexIn[v] == partitionIndexIn[v] && currentIndexOut[v] == partitionIndexOut[v]) {
          newRootSet->push_back(v);
        }
      }
      for (int j = 0; j < outDegree[r]; j++) {
        int v = getOutEdges(r)[j].in_vertex;
        if (vertexColors[v] != -1) {
          continue;
        }
        bool processed = updateIndices(r, v, order, partitionIndexIn, partitionIndexOut,
            currentIndexIn, currentIndexOut, currentIndexInDynamic, currentIndexOutDynamic);
        if (processed && currentIndexIn[v] == partitionIndexIn[v] && currentIndexOut[v] == partitionIndexOut[v]) {
          newRootSet->push_back(v);
        }
      }
    }
    std::vector<int> * tmp = rootSet;
    rootSet = newRootSet;
    newRootSet = tmp;
    iterationCount++;
  }

  printf("Elapsed execution time: %ds\n", rootSetCount);
  return 10;
}
template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::compute_trivial_coloring(){
  vertexColors = (int*)calloc(sizeof(int), vertexCount);
  return 1;
}

// Uses the prefix based method to compute a valid coloring.
template<typename VertexType, typename EdgeType>
int Graph< VertexType,  EdgeType>::compute_coloring(){
  // perform a parallel coloring of the graph.
  std::vector<std::pair<int, int> > permutation(vertexCount);

  cilk_for(int v = 0; v < vertexCount; ++v){
    permutation[v] = std::make_pair(-(inDegree[v] + outDegree[v]), v);
  }
  std::sort(permutation.begin(), permutation.end());

  int* order = (int*) malloc(sizeof(int) * vertexCount);
  vertexColors = (int*) malloc(sizeof(int) * vertexCount);

  cilk_for (int i = 0; i < vertexCount; i++) {
    order[permutation[i].second] = i;
    vertexColors[i] = -1;
  }

  int max_success = 0;
  int prefix_length = 256;
  int num_iterations = 0;
  cilk::reducer_max<int> max_color(-1);
  while (true) {
    bool done = true;
    int stop;
    if (max_success + prefix_length > vertexCount){
      stop = vertexCount;
    } else {
      stop = max_success + prefix_length;
    }

    cilk::reducer_min<int> min(max_success + prefix_length);
    cilk::holder< std::set<int> > neighbor_colors_holder;
    cilk_for(int v = max_success; v < stop; v++) {
      int vid = permutation[v].second;
      if (vertexColors[vid] == -1) {
        min.calc_min(v);
        bool skip = false;
        std::set<int>& neighbor_colors = neighbor_colors_holder();
        neighbor_colors.clear();
        struct edge_info* in_edges = getInEdges(vid);
        struct edge_info* out_edges = getOutEdges(vid);

        for (int i = 0; i < inDegree[vid]; i++) {
          if (order[in_edges[i].out_vertex] < v && vertexColors[in_edges[i].out_vertex] == -1) {
            skip = true;
            break;
          }
          neighbor_colors.insert(vertexColors[in_edges[i].out_vertex]);
        }

        if (!skip) {
          for (int i = 0; i < outDegree[vid]; i++) {
            if (order[out_edges[i].in_vertex] < v && vertexColors[out_edges[i].in_vertex] == -1) {
              skip = true;
              break;
            }
            neighbor_colors.insert(vertexColors[out_edges[i].in_vertex]);
          }
        }

        if (skip) {
          done = false;
        } else {
          int chosen_color = -1;
          for (int j = 0; j < vertexCount; j++ ) {
            if (neighbor_colors.find(j) == neighbor_colors.end()){
              chosen_color = j;
              break;
            }
          }
          max_color.calc_max(chosen_color);
          vertexColors[vid] = chosen_color;
        }
      }
    }
    max_success = min.get_value();
    if (done && max_success > vertexCount) {
      break;
    }
    num_iterations++;
  }

  int the_max_color = max_color.get_value();
  return the_max_color+1;
}

template<typename VertexType, typename EdgeType>
int Graph< VertexType, EdgeType>::compute_coloring_grid(int rows, int cols, int height) {
  vertexColors = (int*) malloc(sizeof(int) * (rows)*(cols)*(height));
  int max_color = 0;
  #pragma cilk grainsize=1
  cilk_for (int z = 0; z < height; z++) {
    int z_offset = (z%2)*4 + 8*(z/2);
    //int z_offset = (z%2)*4;
    for (int y = 0; y < cols; y++) {
      int y_offset = (y%2)*2;
      for (int x = 0; x < rows; x++) {
        int x_offset = x%2;
        int color = x_offset + y_offset + z_offset;
        vertexColors[z*rows*cols+y*rows+x] = color;
        if (color > max_color) max_color = color;
      }
    }
  }
/*
  for (int z = 0; z < height; z++) {
    for (int y = 0; y < cols; y++) {
      for (int x = 0; x < rows; x++) {
        int pixel = rows*cols*z + y*rows + x;
        for (int dz = -1; dz <= 1; dz++) {
          for (int dy = -1; dy <= 1; dy++) {
            for (int dx = -1; dx <=1; dx++) {
              if (z+dz < 0 || z+dz >= height) continue;
              if (y+dy < 0 || y+dy >= cols) continue;
              if (x+dx < 0 || x+dx >= rows) continue;
              int new_x = x+dx;
              int new_y = y+dy;
              int new_z = z+dz;
              int new_pixel = rows*cols*new_z + rows*new_y + new_x;
              if(vertexColors[new_pixel] == vertexColors[pixel] && new_pixel != pixel) {
                printf("pixel color %d: x %d, y %d, z %d || new_pixel color %d: x %d, y %d, z %d\n", vertexColors[pixel],x,y,z,vertexColors[new_pixel],new_x,new_y,new_z);
                assert(false);
              }
            }
          }
        }
      }
    }
  }*/
  return max_color+1;
}
template<typename VertexType, typename EdgeType>
int Graph< VertexType, EdgeType>::compute_coloring_atomiccounter() {
  // Step 1: Give the vertices an order.
  int* counters = (int*) malloc(sizeof(int)*vertexCount);
  std::vector<std::pair<int, int> > permutation(vertexCount);
  cilk_for(int v = 0; v < vertexCount; v++) {
    permutation[v] = std::make_pair(-(inDegree[v] + outDegree[v]), v);
  }
  std::sort(permutation.begin(), permutation.end());

  int* order = (int*) malloc(sizeof(int) * vertexCount);
  vertexColors = (int*) malloc(sizeof(int) * vertexCount);

  cilk_for (int i = 0; i < vertexCount; i++) {
    order[permutation[i].second] = i;
    vertexColors[i] = -1;
    counters[i] = 0;
  }

  // Step 2: Initialize the counters, given the order.
  cilk_for (int i = 0; i < vertexCount; i++) {
    struct edge_info* in_edges = getInEdges(i);
    struct edge_info* out_edges = getOutEdges(i);
    int count = 0;
    for (int j = 0; j < inDegree[i]; j++) {
      int neighbor = in_edges[j].out_vertex;
      if (order[neighbor] < order[i]) {
        count++;
      }
    }
    for (int j = 0; j < outDegree[i]; j++) {
      int neighbor = out_edges[j].in_vertex;
      if (order[neighbor] < order[i]) {
        count++;
      }
    }
    counters[i] = count;
  }
  cilk::holder< std::set<int> > neighbor_set_holder;
  // Step 3: Start the computation
  cilk_for (int v = 0; v < vertexCount; v++) {
    if (counters[v] == 0) {
      asyncColor(v, order, counters, &neighbor_set_holder);
    }
  }
  // compute the maximum color.
  int maxColor = -1;
  for (int i = 0; i < vertexCount; i++) {
    maxColor = vertexColors[i] > maxColor ? vertexColors[i] : maxColor;
  }
  return maxColor + 1;
}

