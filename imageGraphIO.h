// Copyright 2014

#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <cilk/cilk.h>
#include <cilk/reducer.h>
#include <cilk/reducer_opadd.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <string>
#include <vector>

#include "./Graph.h"
#include "./engine.h"
#include "./tfk_utils.h"
#include "./parallel_utils.h"
#include "./vector_reducer.h"

#ifndef IMAGEGRAPHIO_H_
#define IMAGEGRAPHIO_H_

// Convert openCV 2D matrix into row major array.
template<typename T>
T* Mat_to_Row_Major(const cv::Mat& in, int cv_type) {
    T* row_major = static_cast<T*>(malloc(sizeof(T) * in.rows * in.cols));
    for (int row = 0; row < in.rows; row++) {
        cv::Mat tmp2;
        in.row(row).convertTo(tmp2, cv_type);
        T* ptemp = reinterpret_cast<T*>(tmp2.ptr());
        for (int j = 0; j < in.cols; j++) {
          row_major[row*in.cols + j] = ptemp[j];
        }
    }
    return row_major;
}

// Convert openCV 2D matrix into row major array.
template<typename T>
T* Mat_to_Row_Major(const cv::Mat& in) {
    T* row_major = static_cast<T*>(malloc(sizeof(T) * in.rows * in.cols));
    for (int row = 0; row < in.rows; row++) {
        cv::Mat tmp2;
        in.row(row).convertTo(tmp2, CV_8U);
        T* ptemp = static_cast<T*>(tmp2.ptr());
        for (int j = 0; j < in.cols; j++) {
          row_major[row*in.cols + j] = ptemp[j];
        }
    }
    return row_major;
}

// Convert array of row major arrays into graph representing 3d volume.
Graph<vdata, edata>* row_major_to_graph(uint8_t** row_major, int rows,
    int cols, int height) {
  Graph<vdata, edata>* graph = new Graph<vdata, edata>();
  printf("before resize\n");
  graph->resize(rows*cols*height);
  printf("after resize\n");
  printf("after resize vertex count %d\n", graph->vertexCount);

  for (int z = 0; z < height; z++) {
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
        for (int dz = -1; dz <= 1; dz++) {
          if (r+dy < 0 || r+dy >= rows) continue;
          if (c+dx < 0 || c+dx >= cols) continue;
          if (z+dz < 0 || z+dz >= height) continue;

          uint8_t pixel_value = row_major[z][r*cols+c];

          int source = z*rows*cols + r*cols + c;
          int target = (z+dz)*rows*cols + (r+dy)*cols + c + dx;

          if (source != target) {
            // graph->getVertexData(source)->out_degree++;
            // graph->getVertexData(target)->in_degree++;
          } else {
            // graph->getVertexData(source)->r = 0;
            // graph->getVertexData(source)->p = 0;
            graph->getVertexData(source)->x = c;
            graph->getVertexData(source)->y = r;
            graph->getVertexData(source)->z = z;
            graph->getVertexData(source)->pixel_value = pixel_value;
          }
        }
      }
    }
    }
  }
  }
  graph->setGridDimensions(rows, cols, height);
  return graph;
}

cv::Mat colorize(cv::Mat c1, cv::Mat c2, cv::Mat c3, unsigned int channel = 0) {
    std::vector<cv::Mat> channels;
    channels.push_back(c1);
    channels.push_back(c2);
    channels.push_back(c3);
    cv::Mat color;
    cv::merge(channels, color);
    return color;
}

#endif  // IMAGEGRAPHIO_H_
